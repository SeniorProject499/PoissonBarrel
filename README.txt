
0.0 Contents
===============================================================================

    1.0 Installing Prerequisites
    2.0 Running PoissonBarrel


1.0 Installing Prerequisites
===============================================================================

    Our project is written in Python 3 and requires version 3.5 or greater to
    run. If you do not already have Python3.5 installed on your system,
    download and install it with the following link:

        https://www.python.org/downloads/

    Note: When installing Python on Windows, be sure to click the checkbox to
          add Python3.5 to PATH.

    Once Python has been installed, there are several Python packages that need
    to be installed. This is done with the help of pip, which should have been
    installed with Python.

    To install these packages, first open either a command prompt if you are
    running Windows, or a terminal emulator if you are running Linux or MacOS.

    With a console open, navigate to the project directory that is located in
    our release. From there, run the following command:

        pip3.5 install -r requirements.txt


2.0 Running PoissonBarrel
===============================================================================

    To run our project, first open either a command prompt if you are running
    Windows, or a terminal emulator if you are running Linux or MacOS.

    With a console open, navigate to the project directory that is located in
    our release. That directory should contain all of the source files for the
    project. From there, run the following command:

        python3.5 PoissonBarrel.py

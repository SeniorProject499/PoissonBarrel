"""
MainWnd.py: Class MainWnd
    Class for main window and menu bar.
"""

from PyQt5.QtWidgets import QMainWindow, QAction


class MainWnd( QMainWindow ):
    '''
    Class: MainWnd
    Desc: The applications main window
    '''
    
    def __init__( self, appModel ):
        '''MainWnd constructor
        Desc: Initializes the main window
        Parameters:
                    appModel - instance of the application's model
        '''
        
        #call the base class constructor
        super( MainWnd, self ).__init__()
        
        #set the application model
        self.appModel = appModel
        
        #the window's title
        self.wndTitle = "PoissonBarrel"
        
        self.setWindowTitle( self.wndTitle )
        
        #initiate create menu items and their correspond actions
        self.initActions()
        self.initUI()
        
    
    def initUI( self ):
        
        #set the window's title
        
        #create the menu bar
        
        menuBar = self.menuBar()

        #add a file menu
        fileMenu  = menuBar.addMenu( '&File' )     
        
        #add menu items to the file menu
        fileMenu.addAction( self.newAction )
        fileMenu.addAction( self.openAction )
        fileMenu.addAction( self.saveAction )
        fileMenu.addAction( self.saveAsAction )
        fileMenu.addAction( self.exitAction )
        
        
        self.fileMenu = fileMenu
       
        #add a edit menu
        editMenu = menuBar.addMenu( '&Edit' ) 
		
        self.editMenu = editMenu
        
        self.editMenu.addAction( self.tableAction )
	    
        #add a statistical measure menu
        #statMenu = menuBar.addMenu( 'Statistical Measures' )
       
        toolsMenu = menuBar.addMenu( '&Tools' )
        toolsMenu.addAction( self.statMeasureAction)
        graphMenu = toolsMenu.addMenu( 'Graph')

        self.reportMenu = menuBar.addMenu('&Report')
        self.reportMenu.addAction(self.summaryAction)

        helpMenu = menuBar.addMenu( '&Help' )
        helpMenu.addAction( self.helpAction )

        self.toolsMenu = toolsMenu  
		
        #graphMenu.addAction( self.horBarGraphAction )
        #graphMenu.addAction( self.verBarGraphAction )
        graphMenu.addAction( self.barGraphAction )
        graphMenu.addAction( self.xyGraphAction )
        graphMenu.addAction( self.normDistributionCurveAction )
        graphMenu.addAction( self.pieChartAction )
		
		
        
    def initActions(self ):
       
        
        #new document action
        self.newAction = QAction( '&New', self )
        self.newAction.triggered.connect( self.appModel.onNew )
        
        #open file action
        self.openAction = QAction( '&Open', self )
        self.openAction.triggered.connect( self.appModel.onOpen )
        
        #save action
        self.saveAction = QAction( '&Save', self )
        self.saveAction.triggered.connect( self.appModel.onSave )
        
        #save as action
        self.saveAsAction = QAction( '&Save as', self )
        self.saveAsAction.triggered.connect( self.appModel.onSaveAs )
        
        #stat measure dialog action
        self.statMeasureAction = QAction( 'Statistical Measures' )
        self.statMeasureAction.triggered.connect(self.appModel.onStatDialog)

        self.summaryAction = QAction('View Report')
        self.summaryAction.triggered.connect(self.appModel.onSummaryDialog)

        
        #XY graph dialog
        self.xyGraphAction = QAction( '&X-Y Graph ', self )
        self.xyGraphAction.triggered.connect( self.appModel.onXYGraphDialog )
        
        self.barGraphAction = ( QAction( '&Bar Graph', self ) )
        self.barGraphAction.triggered.connect( self.appModel.onBarGraphCreatorDialog )
        
        #normal distribution curve dialog
        self.normDistributionCurveAction = QAction( '&Normal Distribution Curve', self )
        self.normDistributionCurveAction.triggered.connect( self.appModel.onNormDistCurveDialog )
        
        #pie chart dialog
        self.pieChartAction = QAction( '&Pie Chart', self ) 
        self.pieChartAction.triggered.connect( self.appModel.onPieChartDialog )
        
        #help action
        self.helpAction = QAction( 'I Need Help' )
        self.helpAction.triggered.connect( self.appModel.onHelpDialog )
        
        #table editor action
        self.tableAction = QAction( 'Table Properties' )
        self.tableAction.triggered.connect( self.appModel.onTableEdit )
        
        #exit the program action
        self.exitAction = QAction( '&Exit', self )
        self.exitAction.triggered.connect( self.appModel.onExit )
        
        
        
    def addFNameTitle(self, fName ):
        
        self.setWindowTitle( self.wndTitle + ' - ' + fName)
        
    def removeFNameTitel(self):
        self.setWindowTitle( self.wndTitle )
        
        
    def disableMenu(self, enabled):
        
        self.fileMenu.setEnabled( enabled )
        self.editMenu.setEnabled( enabled )
        self.toolsMenu.setEnabled( enabled )
        
    def closeEvent(self, e ):
        self.appModel.onClose( e )
        
    

from dialogs.GraphDialogBase import GraphDialogBase

from PyQt5 import QtWidgets


class BarChart_Dialog( GraphDialogBase ):
    
    def __init__(self, mainWnd, appModel, graph ):
        
        super( BarChart_Dialog, self ).__init__( mainWnd, appModel, graph )
        
        #get the labels and the values
        self.labels = graph.getLabels()
        self.values = graph.getValues()
        
        self.comboBox = QtWidgets.QComboBox()
        #add all labels to the combo
        for label in self.labels:
            self.comboBox.addItem( label )
        
        #create the color button
        self.colorBtn = QtWidgets.QPushButton('Choose Color')
        self.colorBtn.clicked.connect( self.onColorClicked )
        
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget( self.comboBox )
        hBox.addWidget( self.colorBtn )
        self.addOptionLayout( hBox )
        
        
    def onRadioToggled(self):
        pass
        
    
    def onColorClicked(self):
        
        index = self.comboBox.currentIndex()
        
        color = self.getColorFromDialog()
        
        if not color:
            return
        
        self.graph.setLabelColor( index, color )
        



from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QComboBox, QLabel, QPushButton, QLineEdit, QRadioButton, QTextEdit

from CellSelectionEdit import CellSelectionEdit

import TableParsing

from PyQt5.QtGui import QFont


#a base dialog class which encapsulates all interaction between the subclassed dialog
#and the table model
class BaseDialog( QDialog ):
    
    #class constructor
    def __init__(self, parent, appModel, tableModel ):
        
        #call the base class constructor
        super( BaseDialog, self ).__init__( parent )
        
        #set the table and app model
        self.appModel = appModel
        self.tableModel = tableModel
        self.mainWnd = parent
        
        #control font
        self.controlFont = QFont( "Times", 12 )
        
        #the edit that correspond to the check radio button
        self.__selectedEdit__ = None
        
        #the text edit used for displaying error messages display error messages
        self.__errorEdit__ = None
    
    #called when the user presses the return key
    #while a line edit has focus
    def onReturnPressed(self):
        
        focusEdit = None
        
        for edit in self.__radio2EditDict__.values():
            
            if edit.hasFocus():
                focusEdit = edit
                break
            
        if not focusEdit:
            return
        
        headings = focusEdit.text()
        
        if not headings:
            return
        
        self.tableModel.setSelectedCells( headings )
        
    
    #called by the table model when cells are selected
    def update(self):
         
         if not self.__selectedEdit__:
             return
         
         indices = self.tableModel.getSelectedCells()
         
         headingStr = TableParsing.cells2Heading( indices )
         
         self.__selectedEdit__.setText( headingStr )
         
         self.__selectedEdit__.selectAll()
         
         self.show()
     
    
    #create a widget pair consisting of a label
    #and line edit inserted into horizontal layout
    #which is then inserted into the parent layout
    def createSelEdit( self, parentLayout ):
             
        hBox = QHBoxLayout()
        label = QLabel()
        edit = CellSelectionEdit( self )
         
        label.setFont( self.controlFont )
        edit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( edit )
        
        parentLayout.addLayout( hBox )
    
        return label, edit
    
    
    #given a list of cell indices, this method returns corresponding
    #cell data string
    def getCellValues(self, indices ):
        
        strLst = []
        for colIndex, rowIndex in indices:
            
            s = self.tableModel.getCellStr( rowIndex, colIndex )
            
            strLst.append( s )
            
        return strLst
    
    
    #given a list of cell indices and strings this
    #method sets each table cell with its corresponding string
    def setCellData(self, strData, indices  ):
        
        index = 0
        
        dataLen = len( strData )
        
        while index < dataLen:
            colNum, rowNum = indices[ index ]
            self.tableModel.setCellStr( rowNum, colNum, strData[ index ] )
            index += 1
            
    #called by the cell selection that has gained focus
    def notifySelEditFocus(self, edit, hasFocus ):
        
        if hasFocus:
            self.__selectedEdit__ = edit
            
        else:
            self.__selectedEdit__ = None
    
    #called by the table model after the user
    #has finished selecting cells
    def onSelectionTerminated(self):
        
        if self.__selectedEdit__:
            self.__selectedEdit__.activateWindow()
    
    #prohibits the selected edit from responding to
    #cells selections
    def deselectEdit(self):
        if self.__selectedEdit__:
            self.__selectedEdit__.clearFocus()
            self.__selectedEdit__ = None
            
    #designates a text edit to display error message due to invalid input
    def setErrorTextEdit(self, edit ):
        self.__errorEdit__ = edit
        
        edit.setReadOnly( True )
    
    #appends additional error messages to the error text edit
    def displayErrorText(self, errorText ):
        self.__errorEdit__.append( errorText + '\n' )
    
    #removes all error text from the text edit 
    def clearErrorText( self ):
        self.__errorEdit__.clear()
        
    
    #overridden method that notifies the app model when the dialog is closed
    def closeEvent(self, e ):
        self.appModel.onDialogClose()
        
           
"""
SummaryDialog.py: Class SummaryDialog
    Dialog for collection of statistical measure summaries.
"""

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QTextEdit, QApplication, QFileDialog, QMessageBox
from PyQt5 import QtCore
from PyQt5.QtCore import QSize

from dialogs.baseDialog import BaseDialog

class SummaryDialog(BaseDialog):
    
    def __init__( self, parent, appModel, tableModel ):
        super(SummaryDialog, self).__init__(parent, appModel, tableModel)
        self.appModel = appModel
        self.tableModel = tableModel
        self.initUI()
    
    def initUI(self):
        self.hBox = QHBoxLayout()
        self.hBox.addStretch(1)
        self.textbox = QTextEdit() 
        font = self.textbox.font()
        font.setFamily("Courier")
        font.setPointSize(10)
        vscroll = self.textbox.verticalScrollBar()
        vscroll.setValue(vscroll.maximum())
        self.resize(500, 300)
        size = 500
        self.textbox.setMinimumSize(QSize(size, size*0.75))
        self.textbox.setReadOnly(True)
        self.hBox.addWidget(self.textbox)
        self.vBox = QVBoxLayout()
        #self.vBox.addStretch(1)
        
        
        self.vBox.addLayout(self.hBox)
        self.setLayout(self.vBox)
        
        self.saveBtn = QPushButton( 'Save' )
        self.saveBtn.clicked.connect( self.onSave )
        self.closeBtn = QPushButton( 'Close' )
        self.closeBtn.clicked.connect( self.onClose )
        
        hBox = QHBoxLayout()
        hBox.addStretch( 1 )
        hBox.addWidget( self.saveBtn )
        hBox.addWidget( self.closeBtn )
        
        self.vBox.addLayout( hBox )     
        
        self.setWindowTitle("Summary")
        self.setModal(False)
    
    def addSummary(self, summaryText):
        text = self.textbox.toPlainText()
        if (text): # If there is already text...
            self.textbox.setPlainText(text + "\n" + str(summaryText))
        else: # If this is the first text
            self.textbox.setPlainText(str(summaryText))
    
    
    def onSave(self):
        dir = self.appModel.getDir()
        text = self.textbox.toPlainText()
        
        fPath, type = QFileDialog.getSaveFileName(self.mainWnd, 'Save Text File', dir )
        
        if not fPath:
            return
        
        if len( fPath ) < 4 or fPath[ -4: ] != '.txt':
            fPath += '.txt'
            
        try:
            handle = open( fPath, 'w' )
            handle.write( text )
            handle.close()
        
        except:
            msg = QMessageBox()
            msg.setWindowTitle( 'Error' )
            msg.setText( 'Failed to create file' )
            msg.exec_()
    
    def onClose(self):
        self.close()
        
            

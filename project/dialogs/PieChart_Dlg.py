"""
PieChart_Dlg.py: Class PieChart_Dialog
    Dialog for Pie Chart graphs.
"""

from dialogs.GraphDialogBase import GraphDialogBase
from stats.probability_distribution import ProbabilityDistribution

from PyQt5 import QtWidgets


class PieChart_Dialog( GraphDialogBase ):
    
    def __init__(self, mainWnd, appModel, graph ):
        
        super( PieChart_Dialog, self ).__init__( mainWnd, appModel, graph )
        
        #get the labels and the values
        self.labels = graph.getLabels()
        self.values = graph.getValues()
        
        #compute the probability distribution
        dist = ProbabilityDistribution()
        dist.setInput( 'Values', self.values )
        dist.computeResult()
        
        #get the list of values as strings round to 4 decimal places
        vals = list( dist.getOutput( 'Distribution' ) )
        self.distVals = []
        for i in range( len( vals ) ):
            self.distVals.append( str( round( vals[ i ], 4 ) ) )
        
        self.comboBox = QtWidgets.QComboBox()
        
        #add all labels to the combo
        for label in self.labels:
            self.comboBox.addItem( label )
        
        #create the color button
        self.colorBtn = QtWidgets.QPushButton('Choose Color')
        self.colorBtn.clicked.connect( self.onColorClicked )
        
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget( self.comboBox )
        hBox.addWidget( self.colorBtn )
        self.addOptionLayout( hBox )
        
        self.probDistBtn = QtWidgets.QCheckBox( 'Probability Distribution' )
        self.probDistBtn.toggled.connect( self.onRadioToggled )
        
        self.valuesBtn = QtWidgets.QCheckBox( 'Show Vaues' )
        self.valuesBtn.toggled.connect( self.onRadioToggled )
        
        self.addOptionWidget( self.valuesBtn )
        self.addOptionWidget( self.probDistBtn )
        
        
        
        
    def onRadioToggled(self):
        
        labelLst = list( self.labels )
        
        if self.valuesBtn.isChecked():
            
            for i in range( len( labelLst ) ):
                
                labelLst[ i ] = labelLst[ i ] + ' ' + str( self.values[ i ] )
                
                
        if self.probDistBtn.isChecked():
            
            for i in range( len( labelLst ) ):
                
                labelLst[ i ] = labelLst[ i ] + ' ( ' + self.distVals[ i ] + ' )'
        
        self.graph.setLabels( labelLst )
        
    
    def onColorClicked(self):
        
        index = self.comboBox.currentIndex()
        
        color = self.getColorFromDialog()
        
        if not color:
            return
        
        self.graph.setLabelColor( index, color )
        

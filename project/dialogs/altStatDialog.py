
from StatFactory import StatFactory
from dialogs.baseDialog import BaseDialog
from PyQt5 import QtWidgets
import TableParsing

class altStatDialog( BaseDialog ):
    
    def __init__(self, parent, appModel, tableModel):
        
        super( altStatDialog, self ).__init__( parent, appModel, tableModel )
        self.initUI()
        
        self.summaryDialog = appModel.summaryDialog
    
    def initUI(self):
        
        self.vBox = QtWidgets.QVBoxLayout()
        self.setLayout( self.vBox )
        
        self.setWindowTitle( 'Multiple Measure Selections')
        
        self.checkBoxLst = []
        self.checkStateCache = []
        
        self.createRadioBtns()
        self.createCheckBoxes()
        self.createEdits()
        self.createBtns()
        
        errorText = QtWidgets.QTextEdit()
        self.setErrorTextEdit( errorText )
        self.vBox.addWidget( errorText )
        errorText.setFont( self.controlFont )
        
        self.toggleOnce = False
        
    def createRadioBtns(self):
        
        self.allRadio = QtWidgets.QRadioButton( 'Select All Measures' )
        self.allRadio.setFont( self.controlFont )
        self.allRadio.toggled.connect( self.onAllToggled )
        
        self.indivRadio = QtWidgets.QRadioButton( 'Select Individual Measures')
        self.indivRadio.setFont( self.controlFont )
        self.indivRadio.toggled.connect( self.onSelToggled )
        
        self.indivRadio.setChecked( True )
        
        self.vBox.addWidget( self.allRadio )
        self.vBox.addWidget( self.indivRadio )
    
    def createCheckBoxes(self):
        
        statNameLst = [ 'Mean', 'Median', 'Mode', 'Variance', 'Standard Deviation', 'Coefficient of Variance'  ]
        
        for name in statNameLst:
            checkBox = QtWidgets.QCheckBox( name )
            checkBox.setFont( self.controlFont )
            self.checkBoxLst.append( checkBox )
            self.vBox.addWidget( checkBox )
            self.checkStateCache.append( False )
        
    
    def createEdits(self):
        
        label, edit = self.createSelEdit( self.vBox )
        label.setText( 'Input Cells' )
        self.inputEdit = edit
        
        label, edit = self.createSelEdit( self.vBox )
        label.setText( 'Output Cells' )
        self.outputEdit = edit
        
    def createBtns(self):
        
        hBox = QtWidgets.QHBoxLayout()
        computeBtn = QtWidgets.QPushButton( 'Compute' )
        computeBtn.clicked.connect( self.onCompute )
        computeBtn.setFont( self.controlFont )
        
        closeBtn = QtWidgets.QPushButton( 'Close' )
        closeBtn.clicked.connect( self.onClose )
        closeBtn.setFont( self.controlFont )
        
        hBox.addWidget( computeBtn )
        hBox.addWidget( closeBtn )
        
        self.vBox.addLayout( hBox )
        
    def onCompute(self):
        
        statNames = []
        
        factory = StatFactory.getInstance()
        
        self.clearErrorText()
        
        for box in self.checkBoxLst:
            
            if box.isChecked():
                statNames.append( box.text() )
        
        if len( statNames ) == 0:
            self.displayErrorText( 'At least one Statistical Measure must be checked' )
            return
        
        stats = []
        inputVals = None
        
        for name in statNames:
            stats.append( factory.createMeasure( name ) )
        
        try:
            inputVals = self.tableModel.getNumericData( self.inputEdit.text() )
            
        except Exception as err:
            self.displayErrorText( 'Non-numeric input' )
            return
        
        outVals = []
        
        indices = TableParsing.heading2Cells( self.outputEdit.text() )
        
        if not indices:
            self.displayErrorText( 'Invalid Output Cells' )
            return
        
        if len( indices ) > len( stats ):
            self.displayErrorText( 'Not enough outputs cells to store results')
            return
        
        i = 0
        
        for stat in stats:
            
            inNames = stat.getInputNames()
            outNames = stat.getOutputNames()
            
            try:
                stat.setInput( inNames[ 0 ], inputVals )
                stat.compute()
                outVals.append( stat.getOutput( outNames[ 0 ] ) )
                self.summaryDialog.addSummary( stat.summary() )
            
            except Exception as err:
                outVals.append( None )
                self.displayErrorText( "Error computing result: {0}".format( err ) )
        
        for index, val in enumerate( outVals ):
            
            colNum, rowNum = indices[ index ]
            
            if val == None:
                self.tableModel.setCellStr( rowNum, colNum, '' )         
            
            else:
                self.tableModel.setCellStr( rowNum, colNum, str( val ) )
    
    def onClose(self):
        self.close()
        
    def onAllToggled(self):
        
        if not self.allRadio.isChecked():
            return
        
        for index, box in enumerate( self.checkBoxLst ):
            self.checkStateCache[ index ] = box.isChecked()
            box.setChecked( True )
            box.setEnabled( False )
    
    def onSelToggled(self):
        if not self.indivRadio.isChecked():
            return
        
        for index, box in enumerate( self.checkBoxLst ):
            box.setChecked( self.checkStateCache[ index ] )
            box.setEnabled( True )
    
            
            
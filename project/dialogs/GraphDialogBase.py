"""
GraphDialogBase.py: Class GraphDialogBase
    Base class for graph dialogs. 
"""

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QSizePolicy


class GraphDialogBase( QtWidgets.QDialog ):
    
    def __init__(self, mainWnd, appModel, graph ):
        
        super( GraphDialogBase, self ).__init__( mainWnd )
        
        self.mainWnd = mainWnd
        self.graph = graph
        self.appModel = appModel
        
        
        #the dialog's primary layout
        layout = QtWidgets.QVBoxLayout()
        
        #create hbox layout for the canvas and the options panel
        hBox = QtWidgets.QHBoxLayout()
        
        #create a canvas widget to contain the matplotlib figure, 
        #and the options layout
        self.canvas = FigureCanvas( graph.figure )
        
        #prevent the canvas from being resized
        self.canvas.setSizePolicy( QSizePolicy.Fixed, QSizePolicy.Fixed )
        
        hBox.addWidget( self.canvas )
        
        #create a vbox layout to store optional widgets
        self.optionsLayout = QtWidgets.QVBoxLayout()
        
        #create and add a label the options layout
        label = QtWidgets.QLabel( 'Options' )
        self.optionsLayout.addWidget( label )
        
        #add the options layout to the hbox layout
        hBox.addLayout( self.optionsLayout )
        
        #add the box layout to the primary layout
        layout.addLayout( hBox )
        
        #create a hbox layout for the save and close buttons
        hBox = QtWidgets.QHBoxLayout()
        hBox.addStretch( 1 )
        
        #create the save and close button
        self.saveBtn = QtWidgets.QPushButton( 'Save Image' )
        self.saveBtn.clicked.connect( self.onSave )
        
        self.closeBtn = QtWidgets.QPushButton( 'Close' )
        self.closeBtn.clicked.connect( self.onClose )
        
        #add the buttons to the hbox
        hBox.addWidget( self.saveBtn )
        hBox.addWidget( self.closeBtn )
        
        #add the layout to the dialogs primary layout
        layout.addLayout( hBox )
        
        #set dialog's layout
        self.setLayout( layout )
        
        #set the dialog box as modal
        self.setModal( True )
        
    def onSave(self):
        dir = self.appModel.getDir()
        
        
        
        fPath, type = QtWidgets.QFileDialog.getSaveFileName( self.mainWnd, 'Save Image', dir )
        
        if not fPath:
            return
        
        if len( fPath ) < 4 or fPath[ -4: ] != '.jpg':
            fPath += '.jpg'
        
        try:
            self.graph.figure.savefig( fPath  )
        
        except:
            msg = QtWidgets.QMessageBox()
            msg.setWindowTitle( 'Error' )
            msg.setText( 'Failed to create file' )
            msg.exec_()
        
    def getColorFromDialog(self):
        color = QtWidgets.QColorDialog.getColor()
        
        if not color:
            return None
        
        return ( color.red() / 256, color.green() / 256, color.blue() /256 )
        
    
    def onClose(self):
        self.close()
        
    def addOptionWidget(self, widget ):
        self.optionsLayout.addWidget( widget )
        
    def addOptionLayout(self, layout ):
        self.optionsLayout.addLayout( layout )
        
    def closeEvent(self, e ):
        pass
    
"""
XYGraph_Dlg.py: Class XYGraph_Dialog
    Dialog for XYGraph graphs.
"""

from dialogs.GraphDialogBase import GraphDialogBase

from PyQt5 import QtWidgets


class XYGraph_Dialog( GraphDialogBase ):
    
    def __init__(self, mainWnd, appModel, graph ):
        
        super( XYGraph_Dialog, self ).__init__( mainWnd, appModel, graph )
        
        #self.labels = graph.getLabels()
        
        
        self.leastSqrLineBtn = QtWidgets.QCheckBox( 'Least Square Line' )
        self.leastSqrLineBtn.toggled.connect( self.onLSL )
        self.trendLineBtn = QtWidgets.QCheckBox( 'Trend Line' )
        self.trendLineBtn.toggled.connect( self.onTrend )
        self.addOptionWidget( self.leastSqrLineBtn )
        self.addOptionWidget( self.trendLineBtn )
        
        
    def onLSL(self):
        show = False
        if self.leastSqrLineBtn.isChecked():
            show = True
        self.graph.showLstSqrLine( show )
    
    def onTrend(self):
        show = False
        if self.trendLineBtn.isChecked():
            show = True
        self.graph.showTrendLine( show )
"""
HelpDialog.py: Class HelpDialog
    Help system dialog.
"""

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QComboBox, QLabel, QPushButton, QLineEdit, QApplication

from PyQt5 import QtCore
import TableParsing

from dialogs.baseDialog import BaseDialog

class HelpDialog( BaseDialog ):
    
    def __init__( self, parent, appModel, tableModel ):
        
        super(HelpDialog, self).__init__(parent, appModel, tableModel)
        
        self.appModel = appModel
        self.tableModel = tableModel
        
        self.initUI()
    
    def initUI(self):
        
        self.hBox = QHBoxLayout()
        self.hBox.addStretch(1)
        
        helpString = self.getHelp()
        self.helpLabel = QLabel()
        self.helpLabel.setText(self.getHelp())
        self.hBox.addWidget( self.helpLabel )
        
        self.vBox = QVBoxLayout()
        #self.vBox.addStretch(1)
        self.vBox.addLayout(self.hBox)
        self.setLayout(self.vBox)
        
        self.setWindowTitle( "Help" )
        self.setModal( False )
        
    def getHelp(self):
        activeWnd = QApplication.activeWindow()
        focusWidget = QApplication.focusWidget()
        # These print statements were to try to figure out how to access what's in focus.
        # Until we get this working, they will just clutter up stdout.
        #print(activeWnd)
        #print(type(activeWnd))
        #print(focusWidget)
        #print(type(focusWidget))
        if (activeWnd):
            helpString = "Looks like you need help with " + type(activeWnd).__name__
        else:
            helpString = "Yes, I need help too."
        return helpString

    def update(self):
        self.helpLabel.setText(self.getHelp())
        self.focusWidget()
        
            

"""
XYGraph_CreatorDlg.py: Class XYGraph_CreatorDlg
    Creator dialog for XYGraph graphs.
"""

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QTextEdit, QLineEdit, QCheckBox
from dialogs.baseDialog import BaseDialog
from TableError import TableError
from graphs.XYGraph import XYGraph
from dialogs.XYGraph_Dlg import XYGraph_Dialog
import TableParsing


class XYGraph_CreatorDlg( BaseDialog ):
    
    def __init__(self, parent, appModel, tableModel ):
        
        
        super( XYGraph_CreatorDlg, self ).__init__( parent, appModel, tableModel )
        
        self.initUI()
        
        self.legendFormat = 0
        self.dataFormat = 0
        self.tableModel = tableModel
        
        self.legendStrLst = None
        self.numericDataLst = None
        
        
    def initUI(self):
        
        self.setWindowTitle( 'X-Y Graph Creator' )
        
        self.vBox = QVBoxLayout()
        
        self.setLayout( self.vBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'Chart Title' )
        label.setFont( self.controlFont )
        
        self.chartTitleEdit = QLineEdit()
        self.chartTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.chartTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( ' ' )
        label.setFont( self.controlFont )
        
        hBox.addWidget( label )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'X-Axis Title' )
        label.setFont( self.controlFont )
        self.xAxisTitleEdit = QLineEdit()
        self.xAxisTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.xAxisTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'Y-Axis Title' )
        label.setFont( self.controlFont )
        self.yAxisTitleEdit = QLineEdit()
        self.yAxisTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.yAxisTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        hBox.addStretch( 1 )
        
        radio, edit  = self.createSelEdit( self.vBox )
        radio.setText( 'X Data' )
        self.XdataEdit = edit

        radio, edit  = self.createSelEdit( self.vBox )
        radio.setText( 'Y Data' )
        self.YdataEdit = edit
        
        '''
        self.check = QCheckBox("Trendline")
        self.check.setChecked(False)
        hBox.addWidget( self.check )
        '''
        self.createBtn = QPushButton('Create')
        self.createBtn.setFont( self.controlFont )
        self.createBtn.clicked.connect( self.onCreate )
        
        self.cancelBtn = QPushButton('Cancel')
        self.cancelBtn.setFont( self.controlFont )
        self.cancelBtn.clicked.connect( self.onCancel )
        
        hBox.addWidget( self.createBtn )
        hBox.addWidget( self.cancelBtn )
        
        self.vBox.addLayout( hBox )
        
        errorText = QTextEdit()
        self.setErrorTextEdit( errorText )
        
    def onCreate(self):
        
        try:
            x_values = self.tableModel.getNumericData( self.XdataEdit.text() )
            y_values = self.tableModel.getNumericData( self.YdataEdit.text() )
            
        except TableError as err:
            self.displayErrorText(str(err))
            return
            
            
        if len( x_values ) != len( y_values ):
            self.displayErrorText( '# of X values does not match # of Y values' )
            return
        
        xAxisLabel = self.xAxisTitleEdit.text()
        yAxisLabel = self.yAxisTitleEdit.text()
        title = self.chartTitleEdit.text()
        
        if xAxisLabel == '':
            xAxisLabel = 'X-Axis'
        
        if yAxisLabel == '':
            yAxisLabel = 'Y-Axis'
            
        if title == '':
            title = 'X-Y Plot'
            
        graph = XYGraph( title, xAxisLabel, yAxisLabel, x_values, y_values )
        
        graphDialog = XYGraph_Dialog( self.mainWnd, self.appModel, graph )
        
        graphDialog.exec_()
    
    def onCancel(self):
        self.close()
        
        


"""
StatMeasureDialog.py: Class StatMeasureDialog
    Dialog for statistical measure calculation.
"""

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QComboBox, QLabel, QPushButton, QLineEdit, QRadioButton, QTextEdit

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QFont

import TableParsing
from StatFactory import StatFactory

from dialogs.baseDialog import BaseDialog
from dialogs.altStatDialog import altStatDialog


class StatMeasureDialog( BaseDialog ):
    
    def __init__( self, parent, appModel, tableModel ):
        
        super(StatMeasureDialog, self).__init__( parent, appModel, tableModel )
        
        #set the statistical measure factory
        self.statFactory = StatFactory.getInstance()
        
        self.statMeasure = None
        
        #create and set the dialog widgets primary layout manager
        self.vBox = QVBoxLayout()
        self.setLayout( self.vBox )
        
        # Get summary output dialog
        self.summaryDialog = appModel.summaryDialog
        
        #lists of input/output radio button and text box references for the
        #currently selected statistical measure
        self.inputLineEdits = []
        self.inputLabels = []
        
        self.outputLineEdits = []
        self.outputLabels = []
        
        self.selDialog = altStatDialog( parent, appModel, tableModel )
        
        self.initUI()
        
    
    def initUI(self):
        
        #create the combo box
        self.initCombo()
        
        #add label marking outputs
        label = QLabel( 'Inputs' )
        label.setFont( self.controlFont )
        self.vBox.addWidget( label )
        
        #add the layout for the input widgets 
        self.statInputVBox = QVBoxLayout()
        self.vBox.addLayout( self.statInputVBox )
        
        #add label marking outputs
        label = QLabel( 'Outputs' )
        label.setFont( self.controlFont )
        self.vBox.addWidget( label )
        
        #add the layout for the output widgets
        self.statOutputVBox = QVBoxLayout()
        self.vBox.addLayout( self.statOutputVBox )
        
        #initiate the push button widgets
        self.initBtns()
        
        self.setWindowTitle("Compute a Statistic")
        self.setModal( False )
        
        
        errorEdit = QTextEdit()
        errorEdit.setFont( self.controlFont )
        
        self.vBox.addWidget( errorEdit )
        
        self.setErrorTextEdit( errorEdit )
        
        
    def initCombo(self):
        
        #create combox and add stat measure names
        self.measureCombo = QComboBox( )
        
        #set the font of the combo
        self.measureCombo.setFont( self.controlFont )
        
        #get the measure name
        statNames = self.statFactory.getStatNames()
        
        #add an empty string to the combo box
        self.measureCombo.addItem( '' )
        
        #add each name to the combo box
        for name in statNames: self.measureCombo.addItem( name )
        
        
        #allow the dialog to respond to the user selections
        self.measureCombo.activated[ str ].connect( self.onComboActivated )
         
        
        #create a horizontal layout manager
        hCombo = QHBoxLayout()
        hCombo.addStretch( 1 )
        
        comboLabel = QLabel( "Measure Selection" )
        comboLabel.setFont( self.controlFont )
        
        #add the label and combo box to the h-layout 
        hCombo.addWidget( comboLabel )
        hCombo.addWidget( self.measureCombo )
        
        #add the horizontal layout the dialog primary layout
        self.vBox.addLayout( hCombo )
    
    #called when the user selects a statistical measure
    def onComboActivated(self, text ):
        
        #make a input/output widgets invisible
        self.setIOWidgetsVisible( False )

        for lineEdit in self.inputLineEdits:
            lineEdit.setStyleSheet("background-color: rgb(255, 255, 255);")

        for lineEdit in self.outputLineEdits:
            lineEdit.setStyleSheet("background-color: rgb(255, 255, 255);")
        
        if text == '':
            return
        
        #create the statistical measure via the factory
        self.statMeasure = self.statFactory.createMeasure( text )
        
        #get a list of name of the inputs and outputs of the statistical measure
        statInputs = self.statMeasure.getInputNames()
        statOutputs = self.statMeasure.getOutputNames()
        
        #add additional input and output widgets if needed
        self.add_InWidgets( statInputs )
        self.add_OutWidgets( statOutputs )
        
        #make input and output widgets visible
        self.setIOWidgets( statInputs, statOutputs )
    
    
    #creates additional input widgets: radio buttons and line edits
    def add_InWidgets(self, inputNames  ):
        
        if len( inputNames ) <= len( self.inputLineEdits):
            return
        
        diff = len( inputNames ) - len( self.inputLineEdits )
        
        for i in range( diff ):
            
            label, edit = self.createSelEdit( self.statInputVBox )
            
            self.inputLabels.append( label )
            self.inputLineEdits.append( edit )
    
    #creates additional output widgets
    def add_OutWidgets(self, outNames  ):
        
        if len( outNames ) <= len( self.outputLineEdits):
            return
        
        diff = len( outNames ) - len( self.outputLineEdits )
        
        for i in range( diff ):
            
            label, edit = self.createSelEdit( self.statOutputVBox )
            
            self.outputLabels.append( label )
            self.outputLineEdits.append( edit )
            
            
    
    #makes all i/o widgets visible or invisible
    def setIOWidgetsVisible(self, isVisible ):
        
        
        
        for label in self.inputLabels:
            label.setVisible( isVisible )
        
        for edit in self.inputLineEdits:
            edit.setVisible( isVisible )
            
        for label in self.outputLabels:
            label.setVisible( isVisible )
            
        for edit in self.outputLineEdits:
            edit.setVisible( isVisible )
            
    
    #set the table io widgets for the current statistal measure
    def setIOWidgets(self, inputNames, outputNames ):
        
        index = 0
        
        numWidgets = len( inputNames )
        
        while index < numWidgets:
            
            label = self.inputLabels[ index ]
            label.setText( inputNames[ index ]  )
            label.setVisible( True )
            
            edit = self.inputLineEdits[ index ]
            edit.setVisible( True )
            edit.setText( '' )
            edit.setStatMeasureInput( self.statMeasure, self.inputLabels[ index ].text(), self.tableModel  )
            
            index += 1
            
        index = 0
        
        numWidgets = len( outputNames )
        
        while index < numWidgets:
            
            label = self.outputLabels[ index ]
            label.setText( outputNames[ index ] )
            label.setVisible( True )
            
            edit = self.outputLineEdits[ index ]
            edit.setVisible( True )
            edit.setText( '' )
            
            index += 1
            
    #creates the ok and cancel buttons
    def initBtns(self):
        
        btnLayout = QHBoxLayout()
        
        btnLayout.addStretch( 1 )
        
        self.okBtn = QPushButton( "Compute" )
        cancelBtn = QPushButton( "Close" )
        selBtn = QPushButton( 'Multiple Selection' )
        
        btnLayout.addWidget( self.okBtn )
        btnLayout.addWidget( cancelBtn )
        btnLayout.addWidget( selBtn )
        
        self.vBox.addLayout( btnLayout )
        
        self.okBtn.clicked.connect( self.onOkClicked )
        cancelBtn.clicked.connect( self.onCancelClicked )
        selBtn.clicked.connect( self.onStatSelDlg )
        
    
             
    #called when the ok button is clicked
    def onOkClicked(self):
        
        if not self.statMeasure:
            return
        
        '''
        if not self.setStatInputs():
            return'''
       
        self.clearErrorText()
        
        try:
            self.statMeasure.computeResult()
            self.setCellOutputs()
            self.summaryDialog.addSummary(self.statMeasure.summary())
        except Exception as e:
            
            #print("Error computing result: {0}".format(e))
            self.displayErrorText( "Error computing result: {0}".format( e ) )
        else:
            #print("Successfully computed result!")
            self.displayErrorText( "Successfully computed result!" )
        
        
    def setStatInputs(self):
        
        index = 0
        
        #set the inputs
        for lineEdit in self.inputLineEdits:
            
            #were done setting input if an invisible
            #line edit has been reached
            if not lineEdit.isVisible():
                break
            
            #get the cell headings from the text in the line edit
            cellHeading = lineEdit.text()
            
            #inputs cannot be empty
            if cellHeading == '':
                return False
            
            #covert the cell indices to 
            indices = TableParsing.heading2Cells( cellHeading )
            
            #make sure the indices are valid
            if not indices:
                return False
            
            #get the cell values
            #vals = self.getCellValues( indices )
            
            strVals = self.getCellValues( indices )
            
            #determine the input type
            
            #convert to float for now
            vals = []
            for s in strVals:
                try:
                    vals.append( float( s ) )
                    
                except:
                    vals.append( s )
            
            
            if not vals:
                return
            
            #get the name of the input from the corresponding radio button
            inputStr = self.inputLabels[ index ].text()
            
            #get the inputs type
            
            #set the input
            try:
                self.statMeasure.setInput(inputStr, vals)
                lineEdit.setStyleSheet("background-color: rgb(100, 220, 100);")
                # set text box as green
            except Exception as e:
                self.displayErrorText("{}".format(e))
                lineEdit.setStyleSheet("background-color: rgb(255, 100, 100);")
                return False
            
            #increment the index
            index += 1
            
        return True
            
    
    #sets cells to the outputs of the statistical measure
    def setCellOutputs(self):
        
        index = 0
        
        for lineEdit in self.outputLineEdits:
            
            if not lineEdit.isVisible():
                break
            
            cellHeadings = lineEdit.text()
            
            #convert the heading to cell indices
            indices = TableParsing.heading2Cells( cellHeadings )
            
            #make sure indices are valid
            if not indices:
                return
            
            #get the outputs name
            outputName = self.outputLabels[ index ].text()
            
            #get the output values
            output = self.statMeasure.getOutput( outputName )
            
            # convert output to list if it's a single value
            if not isinstance(output, list):
                output = [output]
                
            if len(indices) < len(output):
                self.displayErrorText("Error not enough cells for output")
                lineEdit.setStyleSheet("background-color: rgb(255, 100, 100);")
                return

            lineEdit.setStyleSheet("background-color: rgb(100, 220, 100);")
            #what = map(str, output)
            self.setCellData(list(map(str, output)), indices)
            index += 1
            
    def onStatSelDlg(self):
        self.close()
        
        self.tableModel.setActiveDialog( self.selDialog )
        self.selDialog.show()
        
    
    def onCancelClicked(self):
        self.close()

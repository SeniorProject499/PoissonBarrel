"""
PieChart_CreatorDlg.py: Class PieChart_CreatorDlg
    Creator dialog for Pie Chart graphs.
"""

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QTextEdit, QLineEdit
from dialogs.baseDialog import BaseDialog
from TableError import TableError
from graphs.PieChart import PieChart
from dialogs.PieChart_Dlg import PieChart_Dialog
import TableParsing



class PieChart_CreatorDlg( BaseDialog ):
    
    def __init__(self, parent, appModel, tableModel ):
        
        
        super( PieChart_CreatorDlg, self ).__init__( parent, appModel, tableModel )
        
        self.initUI()
        
        self.legendFormat = 0
        self.dataFormat = 0
        
        self.legendStrLst = None
        self.numericDataLst = None
        
        
    def initUI(self):
        
        self.setWindowTitle( 'Pie Chart Creator' )
        
        self.vBox = QVBoxLayout()
        
        self.setLayout( self.vBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'Chart Title' )
        label.setFont( self.controlFont )
        
        self.titleEdit = QLineEdit()
        self.titleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.titleEdit )
        
        self.vBox.addLayout( hBox )
        
        label, edit = self.createSelEdit( self.vBox )
        label.setText( 'Labels' )
        self.legendEdit = edit
        
        label, edit = self.createSelEdit( self.vBox )
        label.setText( 'Data' )
        self.dataEdit = edit
        
        hBox = QHBoxLayout()
        hBox.addStretch( 1 )
        
        self.createBtn = QPushButton( 'Create')
        self.createBtn.setFont( self.controlFont )
        self.createBtn.clicked.connect( self.onCreate )
        
        self.cancelBtn = QPushButton( 'Cancel' )
        self.cancelBtn.setFont( self.controlFont )
        self.cancelBtn.clicked.connect( self.onCancel )
        
        hBox.addWidget( self.createBtn )
        hBox.addWidget( self.cancelBtn )
        
        self.vBox.addLayout( hBox )
        
        label = QLabel( 'Error Message' )
        label.setFont( self.controlFont )
        
        self.vBox.addWidget( label )
        
        edit = QTextEdit()
        edit.setFont( self.controlFont )
        self.vBox.addWidget( edit )
        
        self.setErrorTextEdit( edit )
        
    def validate(self):
        
        self.clearErrorText()
        self.legendStrLst = None
        self.numericDataLst = None
        isLegendValid = self.validateLegendLabels()
        isDataValid = self.validateData()
        
        if not isLegendValid or not isDataValid:
            return False
        
        if not self.dataFormat and not self.legendFormat:
            self.displayErrorText( 'Cell formations must be the same' )
            return False
        
        if self.dataFormat == 0:
            self.displayErrorText( 'Inputs must consist of single rows or columns')
            return False
        
        
        if len( self.legendStrLst ) != len( self.numericDataLst ):
            self.displayErrorText( 'The Number of Legend Labels must match to the number of Data Values')
            return False
        
        
        self.displayErrorText( 'Input Cells successfully validated.')
        return True
    
    
    def validateLegendLabels(self):
        
        legendHeading = self.legendEdit.text()
        
        try:
            self.legendStrLst = self.tableModel.getStringData( legendHeading )
            
        except TableError as err:
            self.displayErrorText(str(err))
            return False
        
        self.legendFormat = self.getFormat( legendHeading )
        
        return True
    
    
    def validateData(self):
        
        dataHeading = self.dataEdit.text()
        
        try:
            self.numericDataLst = self.tableModel.getNumericData( dataHeading )
            
        except TableError as err:
            self.displayErrorText(str(err))
            return False
        
        self.dataFormat = self.getFormat( dataHeading )
        
        return True
        
        
    def getFormat(self, headingStr ):
        
        if TableParsing.isRow( headingStr ):
            return 1
        
        elif TableParsing.isCol( headingStr ):
            return 2
        
        else:
            return 0
        
    def onCreate(self):
        if not self.validate():
            return
        
        titleTxt = self.titleEdit.text()
        
        self.close()
        
        pieChart = PieChart(titleTxt, self.legendStrLst, self.numericDataLst )
        
        chartDialog = PieChart_Dialog( self.mainWnd, self.appModel, pieChart  )
        
        chartDialog.exec_()
        
    
    def onCancel(self):
        self.close()

"""
BarChart_CreatorDlg.py: Class BarChart_CreatorDlg
    Creator dialog for Bar Chart graphs.
"""

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QTextEdit, QLineEdit, QComboBox
from dialogs.baseDialog import BaseDialog
from TableError import TableError
from graphs.BarChart import BarChart
from dialogs.BarChart_Dlg import BarChart_Dialog
import TableParsing
import numpy as np


FORMAT_ROW = 1
FORMAT_COLUMN = 2


class BarChart_CreatorDlg( BaseDialog ):
    
    def __init__(self, parent, appModel, tableModel ):
        
        
        super( BarChart_CreatorDlg, self ).__init__( parent, appModel, tableModel )
        
        self.initUI()
        
        self.legendFormat = 0
        self.dataFormat = 0
        
        self.legendStrLst = None
        self.numericDataLst = None
        
        
    def initUI(self):
        
        self.setWindowTitle( 'Bar Chart Creator')
        
        self.vBox = QVBoxLayout()
        
        self.setLayout( self.vBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'Chart Title' )
        label.setFont( self.controlFont )
        
        self.chartTitleEdit = QLineEdit()
        self.chartTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.chartTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'Bar Chart Type' )
        label.setFont( self.controlFont )
        
        self.comboBox = QComboBox()
        self.comboBox.setFont( self.controlFont )
        
        self.comboBox.addItem( 'Vertical' )
        self.comboBox.addItem( 'Horizontal' )
        
        hBox.addWidget( label )
        hBox.addWidget( self.comboBox )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'X-Axis Title' )
        label.setFont( self.controlFont )
        self.xAxisTitleEdit = QLineEdit()
        self.xAxisTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.xAxisTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'Y-Axis Title' )
        label.setFont( self.controlFont )
        self.yAxisTitleEdit = QLineEdit()
        self.yAxisTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.yAxisTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        hBox.addStretch( 1 )
        
        label, edit = self.createSelEdit( self.vBox )
        label.setText( 'Heading Labels' )
        self.dataLabelsEdit = edit
        
        label, edit = self.createSelEdit( self.vBox )
        label.setText( 'Legend Labels' )
        self.legendEdit = edit
        
        label, edit  = self.createSelEdit( self.vBox )
        label.setText( 'Data' )
        self.dataEdit = edit
         
        
        self.createBtn = QPushButton('Create')
        self.createBtn.setFont( self.controlFont )
        self.createBtn.clicked.connect( self.onCreate )
        
        self.cancelBtn = QPushButton('Cancel')
        self.cancelBtn.setFont( self.controlFont )
        self.cancelBtn.clicked.connect( self.onCancel )
        
        hBox.addWidget( self.createBtn )
        hBox.addWidget( self.cancelBtn )
        
        self.vBox.addLayout( hBox )
        
        label = QLabel( 'Error Message' )
        label.setFont( self.controlFont )
        
        self.vBox.addWidget( label )
        
        edit = QTextEdit()
        edit.setFont( self.controlFont )
        self.vBox.addWidget( edit )
        
        self.setErrorTextEdit( edit )
        
    def validate(self):
        
        self.clearErrorText()
        self.legendStrLst = None
        self.numericDataLst = None
        isLegendValid = self.validateLegendLabels()
        isDataValid = self.validateData()
        dataLabelsValid = self.validateDataLabels()
        
        if not isLegendValid or not isDataValid or not dataLabelsValid:
            return False
        
        '''
        if not self.dataFormat and not self.legendFormat:
            self.displayErrorText( 'Cell formations must consist of either a column or a row' )
            return False'''
        
        if not self.dataLabelsFormat:
            self.displayErrorText( 'Data Heading must be either a row or a column' )
            return False
            
        if not self.legendFormat:
            self.displayErrorText( 'Legend Heading must be either a row or a column' )
            return False
            
        
        if self.legendFormat == self.dataLabelsFormat:
            self.displayErrorText( 'Data and Legend Headings cannot be of the same formation' )
            return False
            
        
        if self.dataLabelsFormat == FORMAT_COLUMN:
            
            mat = np.transpose( np.array( self.numericDataLst ) )
            
            self.numericDataLst = []
            
            for row in mat:
                self.numericDataLst.append( list( row ) )
            
        
        if len( self.legendStrLst ) != len( self.numericDataLst ):
            self.displayErrorText( 'The Number of Legend Labels must match to the number of Data Values')
            return False
        
        return True
    
    
    def validateLegendLabels(self):
        
        legendHeading = self.legendEdit.text()
        
        try:
            self.legendStrLst = self.tableModel.getStringData( legendHeading )
            
        except TableError as err:
            self.displayErrorText(str(err))
            return False
        
        self.legendFormat = self.getFormat( legendHeading )
        
        return True

    def validateDataLabels(self):
        
        dataLabelsHeading = self.dataLabelsEdit.text()
        
        try:
            self.dataLabelsLst = self.tableModel.getStringData( dataLabelsHeading )
            
        except TableError as err:
            self.displayErrorText(str(err))
            return False
        
        self.dataLabelsFormat = self.getFormat( dataLabelsHeading )
        
        return True
    
    
    def validateData(self):
        
        dataHeading = self.dataEdit.text()
        
        try:
            self.numericDataLst = self.tableModel.getNumericRegionData( dataHeading )
            
        except TableError as err:
            self.displayErrorText(str(err))
            return False
        
        #self.dataFormat = self.getFormat( dataHeading )
        
        return True
        
        
    def getFormat(self, headingStr ):
        
        if TableParsing.isRow( headingStr ):
            return FORMAT_ROW
        
        elif TableParsing.isCol( headingStr ):
            return FORMAT_COLUMN
        
        else:
            return 0
        
    def onCreate(self):
        if not self.validate():
            return
        
        chartTitle = self.chartTitleEdit.text()
        xAxisTitle = self.xAxisTitleEdit.text()
        yAxisTitle = self.yAxisTitleEdit.text()
        dataLabels = self.dataLabelsEdit.text()
        isVertical = True if self.comboBox.currentText() == "Vertical" else False
        
        self.close()
        
        chart = BarChart(chartTitle, xAxisTitle, yAxisTitle, self.dataLabelsLst,
            self.legendStrLst, self.numericDataLst, isVertical)
        
        chartDialog = BarChart_Dialog( self.mainWnd, self.appModel, chart  )
        
        chartDialog.exec_()
        
    
    def onCancel(self):
        self.close()

"""
NormDistCurve_CreatorDlg.py: Class NormDistCurve_CreatorDlg
    Creator dialog for Normal Distribution Curve graphs.
"""

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QLineEdit, QRadioButton, QTextEdit
from PyQt5.QtGui import QFont
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.mlab as mlab
import math
from TableError import TableError
from dialogs.NormDistCurve_Dlg import NormDistCurve_Dialog
from graphs.NormalDistributionCurve import NormalDistributionCurve

import TableParsing

from dialogs.baseDialog import BaseDialog


class NormDistCurve_CreatorDlg( BaseDialog ):
    
    def __init__(self, parent, appModel, tableModel ):
        
        super( NormDistCurve_CreatorDlg, self ).__init__( parent, appModel, tableModel )
        
        self.tableModel = tableModel
        self.appModel = appModel
        
        self.initUI()
        
        
    def initUI(self):
        
        self.setWindowTitle( 'Normal Distribution Curve Creator')
        
        self.vBox = QVBoxLayout()
        
        self.setLayout( self.vBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'Chart Title' )
        label.setFont( self.controlFont )
        
        self.chartTitleEdit = QLineEdit()
        self.chartTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.chartTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( ' ' )
        label.setFont( self.controlFont )
        
        hBox.addWidget( label )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'X-Axis Title' )
        label.setFont( self.controlFont )
        self.xAxisTitleEdit = QLineEdit()
        self.xAxisTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.xAxisTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        
        label = QLabel( 'Y-Axis Title' )
        label.setFont( self.controlFont )
        self.yAxisTitleEdit = QLineEdit()
        self.yAxisTitleEdit.setFont( self.controlFont )
        
        hBox.addWidget( label )
        hBox.addWidget( self.yAxisTitleEdit )
        
        self.vBox.addLayout( hBox )
        
        hBox = QHBoxLayout()
        hBox.addStretch( 1 )
        
        radio, edit  = self.createSelEdit( self.vBox )
        radio.setText( 'Data' )
        self.dataEdit = edit
         
        
        self.createBtn = QPushButton('Create')
        self.createBtn.setFont( self.controlFont )
        self.createBtn.clicked.connect( self.onCreate )
        
        self.cancelBtn = QPushButton('Cancel')
        self.cancelBtn.setFont( self.controlFont )
        self.cancelBtn.clicked.connect( self.onCancel )
        
        hBox.addWidget( self.createBtn )
        hBox.addWidget( self.cancelBtn )
        
        errorText = QTextEdit()
        errorText.setFont( self.controlFont )
        self.vBox.addWidget( errorText)
        
        self.setErrorTextEdit( errorText )
        
        self.vBox.addLayout( hBox )
        
    def onCreate(self):

        try:
            values = self.tableModel.getNumericData( self.dataEdit.text() )

        except TableError as err:
            self.displayErrorText(str(err))
            return

        xAxisLabel = self.xAxisTitleEdit.text()
        yAxisLabel = self.yAxisTitleEdit.text()
        title = self.chartTitleEdit.text()

        if xAxisLabel == '':
            xAxisLabel = 'X-Axis'
        
        if yAxisLabel == '':
            yAxisLabel = 'Y-Axis'
            
        if title == '':
            title = 'Normal Distribution Curve'

        graph = NormalDistributionCurve( title, xAxisLabel, yAxisLabel, values )
        
        graphDialog = NormDistCurve_Dialog( self.mainWnd, self.appModel, graph )
        
        graphDialog.exec_()

    
    def onCancel(self):
        self.close()

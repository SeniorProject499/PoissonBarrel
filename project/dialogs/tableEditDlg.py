

from PyQt5 import QtWidgets

class TableEdit_Dlg( QtWidgets.QDialog ):
    
    def __init__(self, mainWnd, decPlaces ):
        
        super( TableEdit_Dlg, self ).__init__( mainWnd )
        self.decPlaces = decPlaces
        
        self.initUI()
        self.setModal( True )
    
    def initUI(self):
        
        vBox = QtWidgets.QVBoxLayout()
        
        self.rowSpin = QtWidgets.QSpinBox()
        self.colSpin = QtWidgets.QSpinBox()
        
        self.rowSpin.setMinimum( 0 )
        self.colSpin.setMinimum( 0 )
        
        self.setWindowTitle( 'Table Properties' )
        
        label = QtWidgets.QLabel( 'Add Additional Rows and Columns' )
        
        vBox.addWidget( label )
        
        rowLabel = QtWidgets.QLabel( 'Rows')
        colLabel = QtWidgets.QLabel( 'Columns')
        
        hBox = QtWidgets.QHBoxLayout()
        
        hBox.addWidget( rowLabel )
        hBox.addWidget( self.rowSpin )
        
        vBox.addLayout( hBox )
        
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget( colLabel )
        hBox.addWidget( self.colSpin )
        
        vBox.addLayout( hBox )
        
        #vBox.addWidget( self.rowSpin )
        
        label = QtWidgets.QLabel( 'Decimal Places Displayed' )
        self.decSpin = QtWidgets.QSpinBox()
        self.decSpin.setMinimum( 1 )
        self.decSpin.setMaximum( 10 )
        self.decSpin.setValue( self.decPlaces )
        
        hBox = QtWidgets.QHBoxLayout()
        hBox.addWidget( label )
        hBox.addWidget( self.decSpin )
        
        vBox.addLayout( hBox )
        
        okBtn = QtWidgets.QPushButton( 'Ok' )
        cancelBtn = QtWidgets.QPushButton('Cancel' )
        
        okBtn.clicked.connect( self.accept )
        cancelBtn.clicked.connect( self.reject )
        
        hBox = QtWidgets.QHBoxLayout()
        hBox.addStretch( 1 )
        
        hBox.addWidget( okBtn )
        hBox.addWidget( cancelBtn )
        
        vBox.addLayout( hBox )
        
        self.setLayout( vBox )
        
    
    def getNumCols(self):
        return self.colSpin.value()
    
    def getNumRows(self):
        return self.rowSpin.value()
    
    def getDecPlaces(self):
        return self.decSpin.value()
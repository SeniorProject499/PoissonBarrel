"""
StatFactory.py: Class StatFactory
	The statistical measure factory.
"""

import importlib
import pkgutil
import stats

for (module_loader, name, ispkg) in pkgutil.iter_modules(["stats"]):
    importlib.import_module('stats.' + name, __package__)


"""The statistical measure factory.
"""
class StatFactory( object ):
    __singleton__ = None

    """Initializes the list of statistical measures.
    """
    def __init__(self):
    	
        self.statMeasures = stats.base.StatisticalMethod.__subclasses__()
        self.statNameList = [stat.__stat_name__ for stat in self.statMeasures]
        

    """Returns list of all statistical measure names.
    """
    def getStatNames(self):
        return sorted(list(self.statNameList))
    

    """Creates a statistical measure for a give name.
    """
    def createMeasure(self, statName):
        for stat in self.statMeasures:
            if stat.__stat_name__ == statName:
                return stat()

        raise KeyError("statistical measure '{}' not found".format(statName))
        

    """Returns the singleton instance of StatFactory.
    """
    @staticmethod
    def getInstance():
        if not StatFactory.__singleton__:
            StatFactory.__singleton__ = StatFactory()
            
        return StatFactory.__singleton__ 
            
        

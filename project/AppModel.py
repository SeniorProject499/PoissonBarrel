"""
AppModel.py: Class AppModel
    Model for whole application, which initiates most of the system.
"""

from MainWnd import MainWnd
from TableModel import TableModel
from PyQt5.QtWidgets import QFileDialog, QMessageBox, QDialog
import CSV_Reader
import CSV_Writer
from dialogs.StatMeasureDialog import StatMeasureDialog
from dialogs.HelpDialog import HelpDialog
from dialogs.SummaryDialog import SummaryDialog
from graphs.HorizontalBarGraph import HorBarGraphDialog
from graphs.VerticalBarGraph import VerBarGraphDialog
from dialogs.BarChart_CreatorDlg import BarChart_CreatorDlg
from dialogs.PieChart_CreatorDlg import PieChart_CreatorDlg
from dialogs.NormDistCurve_CreatorDlg import NormDistCurve_CreatorDlg
from dialogs.XYGraph_CreatorDlg import XYGraph_CreatorDlg
from dialogs.tableEditDlg import TableEdit_Dlg
import os

class AppModel( object ):
    '''Class: AppModel
    Desc: initiates the majority of the system, and handles
    menu item input primarily opening and saving csv files
    '''
    
    def __init__( self ):
        '''Method: AppModel Constructor
        Desc: Initiates the main window, the table's model component,
              comma seperated reader and writer, and sets the table 
              widget as the main window's central component
        Access: Public        
        '''
        
        #create the main window
        self.mainWnd = MainWnd( self )
        
        #create the table model
        self.tableModel = TableModel( self )
        
        #create the csv reader
        self.csvReader = CSV_Reader.CSV_Reader()
        
        #create the csv writer
        self.csvWriter = CSV_Writer.CSV_Writer()
        
        #set the table widget as the main window's central widget
        self.mainWnd.setCentralWidget( self.tableModel.getTableWidget() )
        
        #set the current file directory as home
        self.curDir = '/home'
        
        #the currently opened file's path
        self.curFilePath = None
        
        #the name of the opened file
        self.fName = None
        
        #position and size the window
        self.mainWnd.setGeometry( 50, 50, 800, 600 )

        # Create summary dialog box
        self.summaryDialog = SummaryDialog(self.mainWnd, self, self.tableModel)

        #create the statistical method dialog box
        self.statDialog = StatMeasureDialog( self.mainWnd, self, self.tableModel )

        #create the help method dialog box
        self.helpDialog = HelpDialog( self.mainWnd, self, self.tableModel )
        
        #create a bar chart creator dialog
        self.barChartCreator_Dlg = BarChart_CreatorDlg( self.mainWnd, self, self.tableModel )
        
        #self.horzBarChart = HorBarGraphDialog( self.mainWnd, self, self.tableModel )
        
        #create a normal distribution curve creator dialog
        self. normDistCurveCreator_Dlg = NormDistCurve_CreatorDlg( self.mainWnd, self, self.tableModel )
        
        #self.pieChartDialog = PieChartDialog( self.mainWnd, self, self.tableModel )
        
        self.pieChartCreator_Dlg = PieChart_CreatorDlg( self.mainWnd, self, self.tableModel )

        self.xyGraphCreator_Dlg = XYGraph_CreatorDlg( self.mainWnd, self, self.tableModel )

        #make the main window visible
        self.mainWnd.show()
        #self.summaryDialog.show()
        
    def onNew(self):
        
        if not self.continueAction():
            return
        
        #remove the file name and file path
        self.fName = None
        self.curFilePath = None
        
        #reset the table to its original state
        self.tableModel.resetTable()
        
        #remove the file name from the title
        self.mainWnd.removeFNameTitel()
    
    def onOpen(self):
        
        if not self.continueAction():
            return False
        
        #create open file dialog to obtain the file path, that the user intends
        #to load into the application
        fPath, type = QFileDialog.getOpenFileName( self.mainWnd, 'Open File', self.curDir )
        
        #did the user select a file to be opened, if not exit the function
        if not fPath:
            return
        
        #validate and parse the potential csv data file
        csvData = self.csvReader.readFile( fPath )
        
        #if file content is invalid
        if not csvData:
            self.onOpenFileError( fPath )
            return
            
        #display the data in the application's spreadsheet
        self.tableModel.buildTable( csvData )
        
        #separate the file directory from the file's name 
        self.curDir, self.fName = os.path.split( fPath )
        
        #set the current file path
        self.curFilePath = fPath
        
        #add the opened file name to the title
        self.mainWnd.addFNameTitle( self.fName )
        
    
    def onSave(self):
        
        #if the current file path is unset
        if not self.curFilePath:
            
            #perform a saveAs action instead
            self.onSaveAs()
            
        else:
            #otherwise overwrite the current file
            self.saveFile( self.curFilePath )
        
    
    def onSaveAs(self):
        
        #prompt the user for the saved file's path with a save file dialog box
        fPath, type = QFileDialog.getSaveFileName( self.mainWnd, "Save File", self.curDir )
        
        #did the user provide a file path
        if not fPath:
            return
        
        #save the file
        self.saveFile( fPath )
    
    def onExit(self):
        self.mainWnd.close()
    
    def saveFile(self, filePath ):
        
        #extract the table data from the spreadsheet
        tableData = self.tableModel.extractTableData()
        
        #if the csv writer successfully wrote the file
        if self.csvWriter.writeFile( filePath, tableData ):
            
            #set the curent directory, file name, and path
            self.curDir, self.fName = os.path.split( filePath )
            self.curFilePath = filePath
            
            #add the file name to the main windows title
            self.mainWnd.addFNameTitle( self.fName )
            
            #remove the table's modification flag
            self.tableModel.setModified( False )
            
        
        else:
            #otherwise display a message box indicating that there was a 
            #problem writing the file
            msgBox = QMessageBox()
            msgBox.setText( "Could not open " + filePath )
            msgBox.setWindowTitle( "Error: Failed to Write File" )
            msgBox.exec_()
            
    
    def onStatDialog(self):
        self.tableModel.setActiveDialog( self.statDialog )
        self.statDialog.show()

    def onSummaryDialog(self):
        self.tableModel.setActiveDialog( self.summaryDialog )
        self.summaryDialog.show()
        
    def onHelpDialog(self):
        #print( 'On Help Dialog' )
        self.tableModel.setActiveDialog( self.helpDialog )
        self.helpDialog.show()
        
    def onBarGraphCreatorDialog(self):
        self.tableModel.setActiveDialog( self.barChartCreator_Dlg )
        self.barChartCreator_Dlg.show()
        
        
    def onXYGraphDialog(self):
        print( 'On Graph Dialog' )
        self.tableModel.setActiveDialog( self.xyGraphCreator_Dlg )
        self.xyGraphCreator_Dlg.show()
        
    def onNormDistCurveDialog(self):
        self.tableModel.setActiveDialog( self.normDistCurveCreator_Dlg )
        self.normDistCurveCreator_Dlg.show()
        
    def onPieChartDialog(self):
        #self.tableModel.setActiveDialog( self.pieChartDialog )
        
        #self.pieChartDialog.show()
        
        self.tableModel.setActiveDialog( self.pieChartCreator_Dlg )
        self.pieChartCreator_Dlg.show()
        
        
    def onDialogClose(self):
        self.tableModel.setActiveDialog( None )
    
    def onOpenFileError(self, fPath ):
        
        #get the error that occurred when the csv reader attempted to
        #parse the file
        csvError = self.csvReader.getLastError()
        
        errorStr = ""
        
        if csvError == CSV_Reader.CSV_ERROR_INVALID_FILE_CONTENT:
            
            errorStr = fPath + ' is not a valid csv file.'
            
        elif csvError == CSV_Reader.CSV_ERROR_EMPTY_FILE:
            
            errorStr = fPath + " is empty."
            
        elif csvError == CSV_Reader.CSV_ERROR_INVALID_FILE_PATH:
            
            errorStr = "Could not open " + fPath
        
        #create a message, displaying the open file error
        msgBox = QMessageBox()
        msgBox.setText( errorStr )
        msgBox.setWindowTitle( "Error: Failed to Read File")
        
        #display the message box
        msgBox.exec_()
        
    
    def getDir(self):
        return self.curDir
    
    
    def onTableEdit(self):
        
        
        dialog = TableEdit_Dlg( self.mainWnd, self.tableModel.getDecPrecision() )
        
        code = dialog.exec_()
        
        if code == QDialog.Accepted:
            cols = dialog.getNumCols()
            rows = dialog.getNumRows()
            decPlaces = dialog.getDecPlaces()
            
            self.tableModel.addRows( rows )
            self.tableModel.addColumns( cols )
            self.tableModel.setDecPrecision( decPlaces )
            
    def onClose(self, e ):
        if not self.continueAction():
            e.ignore()
    
    def continueAction(self):
        
        if not self.tableModel.modified():
            return True
        
        msg = QMessageBox()
        
        msgStr = ''
        
        if self.curFilePath:
            msgStr = self.fName + ' has changed. Would you like to save?'
            
        else:
            msgStr = 'Would you like to save changes?'
            
        
        msg.setText( msgStr )
        msg.setStandardButtons( QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel )
        msg.setWindowTitle( 'Save' )
        result = msg.exec_()
        
        if result == QMessageBox.Yes:
            
            if self.fName:
                self.saveFile( self.curFilePath )
            
            else:
                self.onSaveAs()
            
            return True
        
        elif result == QMessageBox.No:
            return True
        
        else:
            return False
"""
CellGrouping.py: Cell grouping functions
"""
from operator import itemgetter


def getAdjacentRowWidths( rowNum, colNum, indexDict ):
    
    rowWidths = []
    
    rowVal = rowNum
    colVal = colNum
    
    offSet = 0
    
    while ( rowVal, colVal ) in indexDict:
        
        del indexDict[ ( rowVal, colVal ) ]
        rowWidths.append( 1 )
        colVal += 1
            
        while( rowVal, colVal ) in indexDict:
            
            del indexDict[ ( rowVal, colVal ) ]
            rowWidths[ offSet ] += 1
            colVal += 1
                
        offSet += 1
        rowVal += 1
        colVal = colNum
        
    return rowWidths


def partitionCells( rowNum, colNum, rowWidths ):
        
    height = len( rowWidths )
    segStack = []
    cellGroups = []
        
        
    leftI = 0
    rightI = rowWidths[ 0 ] - 1
    beginRowI = 0
    endRowI = 0
        
    segStack.append( [ leftI, rightI, beginRowI, endRowI ] )
        
        
    for rowIndex in range( 1, height ):
            
        width = rowWidths[ rowIndex ]
        
        partI = width - 1
        
        index = len( segStack ) - 1
        
        while index >= 0:
            
            leftI, rightI, beginRowI, endRowI = segStack[ index ]
            
            
            if partI < leftI:
                segStack.pop()
                cellGroups.append( [ leftI + colNum, rightI + colNum, beginRowI + rowNum, endRowI + rowNum ]  )
                index -= 1
                continue
                
            elif partI == rightI:
                
                segStack[ index ][ 3 ] = rowIndex
                
                
            elif partI > rightI:
                
                segStack[ index ][ 3 ] = rowIndex
                segStack.append( [ rightI + 1, partI, rowIndex, rowIndex ] )
            
            elif partI < rightI:
                
                
                segStack[ index ][ 1 ] = partI
                segStack[ index ][ 3 ] = rowIndex
                cellGroups.append( [ partI + 1 + colNum, rightI + colNum, beginRowI + rowNum, endRowI + rowNum ] )
                
                
            
            partI = leftI - 1
            index -= 1
                
    
    while len( segStack ) > 0:
        
        leftI, rightI, beginRowI, endRowI = segStack.pop()
        
        cellGroups.append( [ leftI + colNum, rightI + colNum, beginRowI + rowNum, endRowI + rowNum ] )
        
    return cellGroups


def groupIndices( indices ):
    
    indexDict = {}
    
    #insert each index into the dictionary
    for cellIndex in indices: indexDict[ cellIndex ] = None
    
    #sort the indices by the row value, then the column value
    indices = sorted( indices, key = itemgetter( 0, 1 ) )
    
    cellGroups = []
    
    for cellIndex in indices:
        
        if not cellIndex in indexDict:
            continue
        
        #get row and column number of the cell
        rowNum, colNum = cellIndex
        
        rowWidths = getAdjacentRowWidths( rowNum, colNum, indexDict )
        
        adjacentGroups = partitionCells( rowNum, colNum, rowWidths )
        
        for group in adjacentGroups: cellGroups.append( group )
        
    cellGroups = sorted( cellGroups, key = itemgetter( 0, 3 ) )
        
    length = len( cellGroups )
    
    for i in range( 0, length ):
        
        group = cellGroups[ i ]
        
        if group[ 0 ] == group[ 1 ] and group[ 2 ] == group[ 3 ]:
            
            cellGroups[ i ] = [ group[ 0 ], group[ 2 ] ]
    
    return cellGroups
        


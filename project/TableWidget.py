"""
TableWidget.py: Class TableWidget
	TableWidget class for spreadsheet table (wrapper for QTableWidget).
"""

from PyQt5.QtWidgets import QTableWidget

from PyQt5 import QtCore

class TableWidget( QTableWidget ):
    '''Class: TableWidget
    Desc:
    '''
    
    def __init__( self, tableModel ):
        
        super( TableWidget, self ).__init__()
        
        self.tableModel = tableModel
        
        self.itemSelectionChanged.connect( tableModel.onSelectionChange  )
        
        self.itemChanged.connect( self.tableModel.onItemChanged )
        
        self.cellDoubleClicked.connect( tableModel.onCellDoubleClicked )     
        
    def mouseReleaseEvent(self, e ):
        
        super( TableWidget, self ).mouseReleaseEvent( e )
        
        if e.button() == QtCore.Qt.LeftButton:
            
            self.tableModel.onMouseRelease()
            
        
        
    def onCellChanged(self, row, col ):
        pass
        #print( row, col )
        
    def onItemChanged(self, item ):
        pass
        #print( item.row(), item.column(), item.text() )
        
    def onSelectionChange( self ):
        
        print( "Selection Change" )
        
        selIndices = self.selectedIndexes()
        
        for i in selIndices:
            
            print( 'Cell', i.row(), i.column() )
            
            
            
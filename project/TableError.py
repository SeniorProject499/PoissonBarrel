"""
TableError.py: Class TableError
    Exception class for table errors.
"""

"""Custom table exception.
"""
class TableError(Exception):
    pass

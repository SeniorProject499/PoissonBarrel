"""
TableParsing.py
	Functions for parsing table references and headings.
"""

import CellGrouping


def colNum2Heading( colNum ):
    '''
    Desc: converts a column number integer to 
          a column heading string
    Params: colNum - column number integer
    Returns: a column heading
    '''
    
    #unicode value of 'A'
    aChrVal = ord( 'A' )
    
    #a list of character that will be converted into the heading string
    chrLst = []
    
    #1st compute the least significant character in the string
    # 0 = A, 1 = B, 2 = C ... 25 = Z
    val = colNum
    modVal = val % 26
    val = val // 26
    
    #covert the value to a string, and insert into the char list
    chrLst.append( chr( aChrVal + modVal ) )
    
    #the next step is trickier, after column Z is is column AA,
    #column AA = 26, ... AZ = 51, ... the problem is for each character in the
    #heading that is not equal to the least significant character
    #the value corresponding to the character shift by 1, now
    #A = 1, B = 2, C = 3, ... Z = 26,
    while val > 0:
        
        #to deal with the offset subtract 1 from the mod value
        modVal = val % 26 - 1
        val = val // 26
        
        #degenerate case: mod is less than 0
        if modVal == -1:
            
            #handle the case by setting the mod value to
            #the 'Z' character value, and decrement the value
            modVal = 25
            val -= 1
        
        #insert the converted value into a character and insert into
        #the front of the list
        chrLst.insert( 0, chr( aChrVal + modVal ) )
    
    #return a string by concatenating the character list
    return ''.join( chrLst )


def heading2ColNum( heading ):
    '''
    Desc: converts a column heading string
          into its corresponding column number
    Params: heading - column heading string to
                      be coverted to an integer
    Returns: a column number integer
    '''
    
    
    length = len( heading )
    
    aChrVal = ord( 'A' )
    
    if length == 0:
        return
    
    #set the running sum to the value of the least significant character
    i = length - 1
    val = ord( heading[ i ] ) - aChrVal
    
    i -= 1
    
    #power counter
    p = 1
    
    #loop the remaining characters of the string in reverse order
    while i >= 0:
        
        #convert each non-least significant character to its corresponding int value
        #remember that A = 1, B = 2, C = 3, ... Z = 26
        temp = ord( heading[ i ] ) - aChrVal + 1
        
        #degenerate case occurs when the 'Z' character is encountered
        if temp == 26:
            val += 25 ** p * temp + 26
        
        #general case: 'A' to 'Y'
        #multiply the value by the its corresponding power of 26, and add the product
        #to the running sum
        else:
            val += 26 ** p * temp
        
        #increment the power counter, and decrement the index
        p += 1
        i -= 1
    
    #return column number value
    return val


#converts a row number to a row heading
def rowNum2Heading( rowNum ):
    
    return str( rowNum + 1 )

#converts a row heading to a row number
def heading2RowNum( heading ):
    
    return int( heading ) - 1


'''converts a list of 2 element tuples consisting 
   of cell indices into a table heading string'''
def cells2Heading( cellIndices ):
    
    cellStr = ''
    
    cellGroups = CellGrouping.groupIndices( cellIndices )
        
    for group in cellGroups:
        
        groupStr = ''
        
        if len( group ) == 2:
            
            groupStr = colNum2Heading( group[ 0 ] ) + rowNum2Heading( group[ 1 ] )
            
        else:
            
            groupStr = colNum2Heading( group[ 0 ] ) + rowNum2Heading( group[ 2 ] ) + ':' + colNum2Heading( group[ 1 ] ) + rowNum2Heading( group[ 3 ] )
            
        if cellStr == '':
            cellStr = groupStr
            
        else:
            cellStr += ', ' + groupStr
            
    return cellStr

#determines if a strings containing a single character is an alphabetic character
def isAlphaChar( ch ):
    
    if ch >= 'A' and ch <= 'Z':
        return True
    
    return False

#determines if a string containing a single character is a numeric character
def isNumChar( ch ):
    
    if ch >= '0' and ch <= '9':
        return True
    
    return False
    

#helper function called by heading2Cells()
#adds a rectangular cell group to the cell index list
def addCellGroup( cellIndices, begRow, endRow, begCol, endCol ):
    
    if begRow > endRow or begCol > endCol:
        return False
    
    
    for rowI in range( begRow, endRow + 1 ):
        
        for colI in range( begCol, endCol + 1 ):
            
            cellIndices.append( ( rowI, colI ) )
            
    return True
        

'''parses a cell heading string into a list of cell index tuples'''
def heading2Cells( headingStr):
    
    
    #remove white space
    headingStr = headingStr.replace( ' ', '' )
    headingStr = headingStr.replace( '\t', '' )
    
    token = ''
    
    cellIndices = []
    
    alphaState = 1
    numericState = 2
    
    prevCell = None
    
    state = alphaState
    
    alphaLen = 0
    
    for ch in headingStr:
        
        if isAlphaChar( ch ):
            
            #must be in alpha state
            if state == numericState:
                return None
            
            token += ch
            
            alphaLen += 1
            
        elif isNumChar( ch ):
            
            #if the next character is a number, transition
            #to the numeric state
            if state == alphaState:
                state = numericState 
    
            #the must contain at least one alpha char
            #before encountering a numeric char
            if token == '':
                return None
            
            token += ch
            
        elif ch == ':':
            
            #state must be numeric
            if state == alphaState:
                return None
            
            prevCell = ( heading2ColNum( token[ :alphaLen ] ), heading2RowNum( token[ alphaLen: ] ) )
            
            token = ''
            state = alphaState
            alphaLen = 0
            
        elif ch == ',':
            
            #must be in the numeric state
            if state == alphaState:
                return None
            
            #convert the cell value
            cell = ( heading2ColNum( token[ :alphaLen ] ), heading2RowNum( token[ alphaLen: ] ) )
            
            state = alphaState
            token = ''
            alphaLen = 0
            
            
            #was the token proceeded by a colon
            if prevCell:
                
                #determine if the cell index range is valid and insert the cell group to the list
                if not addCellGroup( cellIndices, prevCell[ 0 ], cell[ 0 ], prevCell[ 1 ], cell[ 1 ] ):
                    return False
                
                cell = None
                prevCell = None
            
            #otherwise add the cell to the list
            else:
                cellIndices.append( cell )
                
            
            state = alphaState
            token = ''
            alphaLen = 0
        
        #invalid character
        else:
            
            return None
    
    #after the loop terminates there is at least one
    #cell or group to be converted into indices
    
    #proceeds in the same way as the comma case
    
    #must be in numeric state
    if state == alphaState:
        return False
    
    cell = ( heading2ColNum( token[ :alphaLen ] ), heading2RowNum( token[ alphaLen: ] ) )
    
    
    if prevCell:
        
        if not addCellGroup( cellIndices, prevCell[ 0 ], cell[ 0 ], prevCell[ 1 ], cell[ 1 ] ):
            return False
        
    else:
        cellIndices.append( cell )
        
    return cellIndices


#determines if a list cell heading string can be grouped it a single row
def isRow( heading ):
    
    indices = heading2Cells( heading  )
    
    if not indices:
        return False
    
    prevColNum, rowNum = indices[ 0 ]
    
    for colI, rowI in indices[ 1: ]:
        
        if rowI != rowNum or prevColNum + 1 != colI:
            return False
        
        else:
            prevColNum = colI
        
    return True


#determines if the cell heading string is a single rectangle
#if so returns the cell width and height, otherwise returns None
def getCellHeadingDimensions( heading ):
    
    #determine if the heading is valid
    indices = heading2Cells( heading )
    if not indices:
        None
        
    #remove white space
    heading = heading.replace( ' ', '' )
    heading = heading.replace( '\t', '' )
    
    index = 0
    
    for ch in heading:
        
        #group consists of multiple rectangular groups
        #if a comma is present
        if ch == ',':
            return None
    
    minCol, minRow = indices[ 0 ]
    maxCol, maxRow = indices[ -1 ]
    
    width = maxCol - minCol + 1
    height = maxRow - minRow + 1
    
    return width, height

#determines a group of cells as heading string can be grouped into a a single column
def isCol( heading ):
    
    indices = heading2Cells( heading )
    
    if not indices:
        return False
    
    colNum, prevRowNum = indices[ 0 ]
    
    for colI, rowI in indices[ 1: ]:
        
        if colI != colNum or prevRowNum + 1 != rowI:
            return False
        
        else:
            prevRowNum = rowI
            
    return True
 
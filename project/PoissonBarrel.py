"""
PoissonBarrel.py
    main() function and application entry point
"""

#modify the maplotlib backend to support pyqt5
import matplotlib as mpl
mpl.use( 'Qt5Agg' )

from AppModel import AppModel
from PyQt5.QtWidgets import QApplication

import sys

def main():
    '''
    Function: main()
    Desc: The entry point of the application
    '''
    
    #initialize the qt application
    app = QApplication( sys.argv )
    
    #create the application model
    appModel = AppModel()
    
    #execute the qt application
    sys.exit( app.exec_() )

if __name__ == '__main__':
    main()
"""
NormalDistributionCurve.py: Class NormalDistributionCurve
    Class for generation of Normal Distribution Curve graphs.
"""
from graphs.Graph import Graph
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.mlab as mlab
import math


class NormalDistributionCurve( Graph ):
    
    def __init__(self, titleName, xAxisLabel, yAxisLabel, values):
        
        super( NormalDistributionCurve, self ).__init__( titleName )
        
        
        self.xAxisLabel = xAxisLabel
        self.yAxisLabel = yAxisLabel
        self.values = values
        
        self.mean = sum(self.values)/len(self.values)

        var = 0;
        for val in self.values: var = var + (val - self.mean)*(val - self.mean)
        self.var = var
        self.variance = self.var/len(self.values)
        self.sigma = math.sqrt(self.variance)
        self.neg = self.mean - 3*self.sigma
        self.pos = self.mean + 3*self.sigma
        self.range = np.linspace(self.neg, self.pos, 100)
        self.props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

        self.update()
                
    def update(self):
        
    
        self.plot.clear()
        plt.title( self.titleName )
        plt.xlabel( self.xAxisLabel )
        plt.ylabel( self.yAxisLabel )

        norm = self.plot.plot(self.range,mlab.normpdf(self.range, self.mean, self.sigma))
        leg = self.plot.legend(['Mean = %.3f\nStd. Dev = %.3f'%(self.mean, self.sigma)],shadow=True, fancybox=True, handlelength = 0, handletextpad=0, fontsize=10)
        for item in leg.legendHandles:
            item.set_visible(False)
        self.figure.canvas.draw_idle()
    



    


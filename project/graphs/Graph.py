"""
Graphs.py: Class Graph
    Base class for graphs.
"""

import matplotlib as mpl

from matplotlib import pyplot as plt

from random import random, seed


class Graph( object ):
    
    def __init__(self, titleName ):
        
        self.figure = plt.figure( 1, figsize = ( 8, 8 ) )
        self.plot = self.figure.add_subplot( 111 )
        self.titleName = titleName
        
        plt.title( titleName )
    
    
    def update(self):
        pass
    
    def randomColorLst(self, numColors ):
        
        colorLst = []
        
        seed( None )
        
        for i in range( numColors ):
            
            colorLst.append(( random(), random(), random() ) )
            
        return colorLst
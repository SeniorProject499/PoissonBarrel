
from graphs.Graph import Graph
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np

class BarChart( Graph ):
    
    def __init__( self, titleName, xAxisTitle, yAxisTitle, labels,
            legendLabels, values, isVertical ):
        
        super( BarChart, self ).__init__( titleName )
        
        self.xAxisTitle = xAxisTitle
        self.yAxisTitle = yAxisTitle
        self.labels = labels
        self.legendLabels = legendLabels
        self.values = values
        self.isVertical = isVertical

        self.colors = self.randomColorLst(len(self.legendLabels))

        self.update()
        
        
    def update(self):
    
        self.plot.clear()

        ind = np.arange(len(self.labels))
        edge = (0.2 * (len(self.values)-1))/2

        if self.isVertical:
            plt.xticks(ind, self.labels)
            plt.ylim([0, max(map(max, self.values)) * 1.10])
            for index, values in enumerate(self.values):
                self.plot.bar(ind+(0.2 * index)-edge, values, align="center",
                    width=0.2, color=self.colors[index],
                    label=self.legendLabels[index])
        else:
            plt.yticks(ind, self.labels)
            plt.xlim([0, max(map(max, self.values)) * 1.05])
            for index, values in enumerate(self.values):
                self.plot.barh(ind+(0.2 * index)-edge, values, align="center",
                    height=0.2, color=self.colors[index],
                    label=self.legendLabels[index])

            axes = plt.gca()
            ymin, ymax = axes.get_ylim()
            plt.ylim([ymin, ymax * 1.10])


        plt.title(self.titleName)
        plt.xlabel(self.xAxisTitle)
        plt.ylabel(self.yAxisTitle)

        self.plot.legend(shadow=True, fancybox=True, loc='upper center',
            ncol=len(self.legendLabels))

        self.figure.canvas.draw_idle()
    
    
    def getLabels(self):
        return list(self.legendLabels)
    
    def getValues(self):
        return list(self.values)
        
    def setLabels(self, labels):
        self.labels = labels
        self.update()
        
    def setLabelColor(self, index, color):
        self.colors[index] = color
        self.update()

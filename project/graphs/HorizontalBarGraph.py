'''
Created on Nov 2, 2016

@author: Chelsie
'''

from PyQt5.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QComboBox, QLabel, QPushButton, QLineEdit, QRadioButton

from PyQt5 import QtCore
import TableParsing
from StatFactory import StatFactory
import numpy as np
from pylab import *


class HorBarGraphDialog( QDialog ):
    
    def __init__( self, parent, appModel, tableModel ):
        
        super(HorBarGraphDialog, self).__init__(parent, QtCore.Qt.WindowStaysOnTopHint)
        
        #set the app and table model
        self.appModel = appModel
        self.tableModel = tableModel
        
        #set the statistical measure factor
        self.statFactory = StatFactory.getInstance()
        
        self.statMeasure = None
        
        #create and set the dialog widgets primary layout manager
        self.vBox = QVBoxLayout()
        self.setLayout( self.vBox )
        
        
        self.selectedEdit = None
        
        
        #lists of input/output radio button and text box references for the
        #currently selected statistical measure
        self.inputLineEdits = []
        self.inputRadioBtns = []
        
        
        self.outputLineEdits = []
        self.outputRadioBtns = []
        
        #dictionary mapping radio button to their corresponding
        #line edit
        self.radio2LineEditDict = {}
        
        self.initUI()
        
    
    def initUI(self):
        
        #create the combo box
        self.initCombo()
        
        #add label marking outputs
        label = QLabel( 'Inputs' )
        self.vBox.addWidget( label )
        
        #add the layout for the input widgets 
        self.statInputVBox = QVBoxLayout()
        self.vBox.addLayout( self.statInputVBox )
        
        #add the layout for the output widgets
        self.statOutputVBox = QVBoxLayout()
        self.vBox.addLayout( self.statOutputVBox )
        
        #initiate the push button widgets
        self.initBtns()
        
        self.setWindowTitle( "Horizontal Bar Graph Creator")
        self.setModal( False )
        
        
        
    def initCombo(self):
        
        #create combox and add stat measure names
        self.measureCombo = QComboBox( )
        
        #get the measure name
        statNames = self.statFactory.getStatNames()
        
        #add an empty string to the combo box
        self.measureCombo.addItem( '' )
        
        #add each name to the combo box
        for name in statNames: self.measureCombo.addItem( name )
        
        
        #allow the dialog to respond to the user selections
        self.measureCombo.activated[ str ].connect( self.onComboActivated )
         
        
        #create a horizontal layout manager
        hCombo = QHBoxLayout()
        hCombo.addStretch( 1 )
        
        comboLabel = QLabel( "Measure Selection" )
        
        #add the label and combo box to the h-layout 
        hCombo.addWidget( comboLabel )
        hCombo.addWidget( self.measureCombo )
        
        #add the horizontal layout the dialog primary layout
        self.vBox.addLayout( hCombo )
    
    #called when the user selects a statistical measure
    def onComboActivated(self, text ):
        
        #make a input/output widgets invisible
        self.setIOWidgetsVisible( False )
        
        if text == '':
            return
        
        #create the statistical measure via the factory
        self.statMeasure = self.statFactory.createMeasure( text )
        
        #get a list of name of the inputs and outputs of the statistical measure
        statInputs = self.statMeasure.getInputNames()
        statOutputs = self.statMeasure.getOutputNames()
        
        #add additional input and output widgets if needed
        self.add_InWidgets( statInputs )
        self.add_OutWidgets( statOutputs )
        
        #make input and output widgets visible
        self.setIOWidgets( statInputs, statOutputs )
    
    
    #creates additional input widgets: radio buttons and line edits
    def add_InWidgets(self, inputNames  ):
        
        if len( inputNames ) <= len( self.inputLineEdits):
            return
        
        diff = len( inputNames ) - len( self.inputLineEdits )
        
        for i in range( diff ):
                
            #create a radio button, line edit, and horizontal layout
            radioBtn = QRadioButton( '' )
            edit = QLineEdit()
            layout = QHBoxLayout()
            
            #notify the dialog when a radio button is toggled
            radioBtn.toggled.connect( lambda:self.onRadioBtn( radioBtn ) )
            
            #map the radio button to the line edit
            self.radio2LineEditDict[ radioBtn ] = edit
            
            #add button and edit to its corresponding list
            self.inputRadioBtns.append( radioBtn )
            self.inputLineEdits.append( edit )
            
            #add the widget to h-layout
            layout.addWidget( radioBtn )
            layout.addWidget( edit )
            
            #add the layout to the dialogs primary layout
            self.statInputVBox.addLayout( layout )
            
    def add_OutWidgets(self, outNames  ):
        
        if len( outNames ) <= len( self.outputLineEdits):
            return
        
        diff = len( outNames ) - len( self.outputLineEdits )
        
        for i in range( diff ):
                
            #create a radio button, line edit, and horizontal layout
            radioBtn = QRadioButton( '' )
            edit = QLineEdit()
            layout = QHBoxLayout()
            
            #notify the dialog when a radio button is toggled
            radioBtn.toggled.connect( lambda:self.onRadioBtn( radioBtn ) )
            
            #map the radio button to the line edit
            self.radio2LineEditDict[ radioBtn ] = edit
            
            #add button and edit to its corresponding list
            self.outputRadioBtns.append( radioBtn )
            self.outputLineEdits.append( edit )
            
            #add the widget to h-layout
            layout.addWidget( radioBtn )
            layout.addWidget( edit )
            
            #add the layout to the dialogs primary layout
            self.statOutputVBox.addLayout( layout )
            
    
    #makes all i/o widgets visible or invisible
    def setIOWidgetsVisible(self, isVisible ):
        
        
        for radio in self.inputRadioBtns:
            radio.setVisible( isVisible )
            
        for edit in self.inputLineEdits:
            edit.setVisible( isVisible )
            
            
        for radio in self.outputRadioBtns:
            radio.setVisible( isVisible )
            
        for edit in self.outputLineEdits:
            edit.setVisible( isVisible )
            
    
    def setIOWidgets(self, inputNames, outputNames ):
        
        index = 0
        
        numWidgets = len( inputNames )
        
        if numWidgets > 0:
            self.inputRadioBtns[ 0 ].setChecked( True )
        
        while index < numWidgets:
            
            radio = self.inputRadioBtns[ index ]
            radio.setText( inputNames[ index ]  )
            radio.setVisible( True )
            
            edit = self.inputLineEdits[ index ]
            edit.setVisible( True )
            edit.setText( '' )
            
            index += 1
            
        index = 0
        
        numWidgets = len( outputNames )
        
        while index < numWidgets:
            
            radioBtn = self.outputRadioBtns[ index ]
            radioBtn.setText( outputNames[ index ] )
            radioBtn.setVisible( True )
            
            edit = self.outputLineEdits[ index ]
            edit.setVisible( True )
            edit.setText( '' )
            
            index += 1
            
    #creates the ok and cancel buttons
    def initBtns(self):
        
        btnLayout = QHBoxLayout()
        
        btnLayout.addStretch( 1 )
        
        okBtn = QPushButton( "Create" )
        cancelBtn = QPushButton( "Cancel" )
        
        btnLayout.addWidget( okBtn )
        btnLayout.addWidget( cancelBtn )
        
        self.vBox.addLayout( btnLayout )
        
        okBtn.clicked.connect( self.onOkClicked )
        cancelBtn.clicked.connect( self.onCancelClicked )
        
    
    #called when a radio button is clicked
    def onRadioBtn(self, radioBtn ):
        
        #make the radio button is clicked
        if radioBtn.isChecked():
            
            #set the selected edit corresponding to the check radio button
            self.selectedEdit = self.radio2LineEditDict[ radioBtn ]
            
    
    #given a set of cell indices, this m
    
    def getCellValues(self, cellIndices ):
        
        values = []
        
        for colIndex, rowIndex in cellIndices:
            
            s = self.tableModel.getCellStr( rowIndex, colIndex )
             
            values.append( float( s ) )
            
        return values
            
            
    def setCellValues(self, values, cellIndices ):
        
        vLen = len( values )
        
        for i in range( vLen ):
            
            colNum, rowNum = cellIndices[ i ]
            
            self.tableModel.setCellStr( rowNum, colNum, values[ i ] )
             
             
    def createGraph(self):

        index = 0
        
        #set the inputs
        for lineEdit in self.inputLineEdits:
            
            #were done setting input if an invisible
            #line edit has been reached
            if not lineEdit.isVisible():
                break
            
            #get the cell headings from the text in the line edit
            cellHeading = lineEdit.text()
            
            #inputs cannot be empty
            if cellHeading == '':
                return False
            
            #covert the cell indices to 
            indices = TableParsing.heading2Cells( cellHeading )
            
            #make sure the indices are valid
            if not indices:
                return False
            
            #get the cell values
            vals = self.getCellValues( indices )

            vLen = len( vals )
            threshold =  self.statMeasure.result()
            pos = arange(vLen)+.5    # the bar centers on the y axis

            barh(pos,vals, align='center')
            yticks(pos, (indices))
            xlabel('Values')
            title('')
            plot([threshold, threshold], [0., vLen],  "k--")
        
             
    #called the ok button is clicked
    def onOkClicked(self):
        
        if not self.statMeasure:
            return
        
        if not self.setStatInputs():
            return
        
        
        self.statMeasure.computeResult()
        
        self.setCellOutputs()
        self.createGraph()
        show()
        
    def setStatInputs(self):
        
        index = 0
        
        #set the inputs
        for lineEdit in self.inputLineEdits:
            
            #were done setting input if an invisible
            #line edit has been reached
            if not lineEdit.isVisible():
                break
            
            #get the cell headings from the text in the line edit
            cellHeading = lineEdit.text()
            
            #inputs cannot be empty
            if cellHeading == '':
                return False
            
            #covert the cell indices to 
            indices = TableParsing.heading2Cells( cellHeading )
            
            #make sure the indices are valid
            if not indices:
                return False
            
            #get the cell values
            vals = self.getCellValues( indices )
            
            #get the name of the input from the corresponding radio button
            inputStr = self.inputRadioBtns[ index ].text()
            
            #get the inputs type
            
            #set the input
            self.statMeasure.setInput( inputStr, vals )
            
            #increment the index
            index += 1
            
        return True
            
            
    def setCellOutputs(self):
        
        index = 0
        
        for lineEdit in self.outputLineEdits:
            
            if not lineEdit.isVisible():
                break
            
            cellHeadings = lineEdit.text()
            
            #convert the heading to cell indices
            indices = TableParsing.heading2Cells( cellHeadings )
            
            #make sure indices are valid
            if not indices:
                return
            
            #get the outputs name
            outputName = self.outputRadioBtns[ index ].text()
            
            #get the output values
            outputVals = self.statMeasure.getOutput( outputName )
            
            #determine the type of the outputs
            
            #for now convert them to strings
            outputStrs = []
            for o in outputVals: outputStrs.append( str( o ) ) 
            
            #set cells to the output values
            self.setCellValues(outputStrs, indices )
            
            index += 1
        
    
    def onCancelClicked(self):
        self.close()
        
        
    
    #called by the table model when the user selects a cell
    def update(self):
        
        if not self.selectedEdit:
            return
        
        
        
        #get the cells indices that are currently selected
        cells = self.tableModel.getSelectedCells()
        
        #convert the cell indices to a cell heading string
        cellStr = TableParsing.cells2Heading( cells )
        
        self.selectedEdit.setText( cellStr )

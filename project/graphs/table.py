#!/usr/local/bin/python3

from .base import Graph

import matplotlib
import matplotlib.pyplot as plt; plt.rcdefaults()
import numpy as np
import matplotlib.pyplot as plt

#
#

"""
"""
class TableChart(Graph):

    """ Handles initialization.
    """
    def __init__(self):
        super(Graph, self).__init__()

        self.__inputs__ = {
            "Column Labels": None,
            "Row Labels": None,
            "Data": None
        }


    """Check that all inputs are valid.

    The only input that needs to be checked are the values. The titles can be
    any string, which includes numbers.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "Data":
            if all(l is None for l in value):
                raise TypeError("Empty lists")
            elif min(map(len, value)) != max(map(len, value)):
                raise TypeError("Not all lists are same length")

        elif name == "Column Labels":
            if not all(isinstance(v, str) for v in value):
                raise TypeError("Invalid column label")

        elif name == "Row Labels":
            if not all(isinstance(v, str) for v in value):
                raise TypeError("Invalid row label")


    """This should eventually return a matplotlib graph object to be used
    elsewhere. But for now, just show the graph.
    """
    def getGraph(self):

        col_labels = self.__inputs__["Column Labels"]
        row_labels = self.__inputs__["Row Labels"]
        data = self.__inputs__["Data"]

        plt.table(cellText=data, colWidths = [0.25]*len(col_labels),
            rowLabels=row_labels,
            colLabels=col_labels,
            cellLoc = 'center', rowLoc = 'center',
            loc='center')

        plt.xticks([])
        plt.yticks([])
        plt.title('Loss by Disaster')

        plt.show()


if __name__ == "__main__":
    table = TableChart()

    table.setInput("Column Labels", ["One", "Two", "Three", "7", "8", "9", "10", "11"])
    table.setInput("Row Labels", ["Four", "Five", "Six"])
    table.setInput("Data", [["a"] * 8, ["b"] * 8, ["c"] * 8])

    table.getGraph()

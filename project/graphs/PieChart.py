"""
PieChart.py: Class PieChart
    Class for generation of Pie Chart graphs.
"""

from graphs.Graph import Graph
from matplotlib import pyplot as plt


class PieChart( Graph ):
    
    def __init__(self, titleName, labels, values):
        
        super( PieChart, self ).__init__( titleName )
        
        
        self.labels = labels
        self.values = values
        
        numColors = len( values )
        
        self.cols = self.randomColorLst( numColors )
        
        self.update()
        
    def update(self):
        
    
        self.plot.clear()
        self.plot.pie( self.values, labels = self.getLabels(), colors = self.cols )
        plt.title( self.titleName )
        self.figure.canvas.draw_idle()
    
    
    def getLabels(self ):
        return list( self.labels )
    
    def getValues(self):
        return list( self.values )
        
    def setLabels(self, labels ):
        
        self.labels = labels
        
        self.update()
        
    def setLabelColor(self, index, color ):
        
        self.cols[ index ] = color
        self.update()
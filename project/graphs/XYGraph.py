"""
XYGraph.py: Class XYGraph
    Class for generation of XYGraph graphs.
"""

from graphs.Graph import Graph
from matplotlib import pyplot as plt
from stats.least_square_line import LeastSquareLine
import numpy as np

class XYGraph( Graph ):
    
    def __init__(self, titleName, xAxisLabel, yAxisLabel, xValues, yValues):
        
        super( XYGraph, self ).__init__( titleName )
        
        self.drawTrendLine = False
        self.drawLSL = False
        
        lstSqrLn = LeastSquareLine()
        
        lstSqrLn.setInput( 'X Values', xValues )
        lstSqrLn.setInput( 'Y Values', yValues )
        
        lstSqrLn.computeResult()
        
        self.slope = lstSqrLn.getOutput( 'Slope')
        self.yInt = lstSqrLn.getOutput( 'Y-Intercept' )
        
        self.xAxisLabel = xAxisLabel
        self.yAxisLabel = yAxisLabel
        self.xValues = xValues
        self.yValues = yValues
        
        self.update()
        
        self.minX = min( self.xValues ) - 0.5
        self.maxX = max( self.xValues ) * 1.05
        
        self.minY = min( self.yValues ) - .5
        self.maxY = max( self.yValues ) * 1.05
        
        self.lslXVals = [ self.minX, self.maxX ]
        self.lslYVals = [ self.minX * self.slope + self.yInt, self.maxX * self.slope + self.slope   ]
        
    def update(self):
    
        self.plot.clear()
        plt.title( self.titleName )
        plt.xlabel( self.xAxisLabel )
        plt.ylabel( self.yAxisLabel )
        
        self.plot.plot( self.xValues, self.yValues, 'ro' )
        self.plot.axis( [ (min( self.xValues))-0.5, (max( self.xValues))*1.05, (min( self.xValues))-0.5, (max( self.yValues))*1.05] )
        
        if self.drawTrendLine:
            
            z = np.polyfit( self.xValues, self.yValues, 1 )
            p = np.poly1d( z )
            self.plot.plot( self.xValues, p( self.xValues ), 'r--'  )
        
        
        if self.drawLSL:
            
            self.plot.plot( self.lslXVals, self.lslYVals, 'r--')
        
        self.figure.canvas.draw_idle()
        
    
    def showTrendLine(self, show ):
        
        if show:
            self.drawTrendLine = True
        
        else:
            self.drawTrendLine = False
        
        self.update()
        
    def showLstSqrLine(self, show ):
        
        if show:
            self.drawLSL = True
            
        else:
            self.drawLSL = False
            
        self.update()

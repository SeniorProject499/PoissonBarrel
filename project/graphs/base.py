#!/usr/local/bin/python3

from abc import ABCMeta, abstractmethod

#import numpy as np
#import matplotlib.pyplot as plt


""" Base class for handling graphical things.
"""
class Graph:
    __metaclass__ = ABCMeta

    """ Handles initialization.
    """
    def __init__(self):
        pass

    """ Get a list of input field names.
    """
    def getInputNames(self):
        return list(self.__inputs__.keys())


    """ Method used for setting inputs.
    """
    def setInput(self, name, value):
        if name not in self.__inputs__.keys():
            raise NameError("no such input field: {}".format(name))

        try:
            self.checkInput(name, value)
        except TypeError as e:
            print("Bad value ({}) for input ({}): {}".format(value, name, e))
            raise

        self.__inputs__[name] = value


    """ Abstract method for checking input values.
    """
    @abstractmethod
    def checkInput(self, name, value):
        raise NotImplementedError


    """ Get the method's result.
    """
    @abstractmethod
    def getGraph(self):
        raise NotImplementedError

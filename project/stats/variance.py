"""
variance.py: Class Variance
    Class for Variance statistical method.
"""

from stats.base import StatisticalMethod


"""Statistical method for computing Variance.
"""
class Variance(StatisticalMethod):
    __stat_name__ = "Variance"

    """Setup inputs and outputs.
    """
    def __init__(self):
        super(Variance, self).__init__()
            
        self.__inputs__ = {
            "Values": None
        }

        self.__outputs__ = {
            "Variance": None
        }


    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "Values":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))


    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:
        values = self.__inputs__["Values"]
        mean = sum(values)/len(values)
        data = [(num - mean) ** 2 for num in values]
        variance = sum(data)/(len(data) - 1)

        self.__outputs__["Variance"] = variance

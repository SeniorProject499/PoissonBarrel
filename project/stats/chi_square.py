"""
chi_square.py: Class ChiSquare
    Class for Chi-Square statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Chi-Square.
"""
class ChiSquare(StatisticalMethod):
    __stat_name__ = "Chi-Square"

    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self) -> None:
        super(ChiSquare, self).__init__()

        self.__inputs__ = {
            "Observed": None,
            "Expected": None
        }

        self.__outputs__ = {
            "Chi-Square": None
        }


    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "Observed" or name == "Expected":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))


    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:
        observed = self.__inputs__["Observed"]
        expected = self.__inputs__["Expected"]

        if len(observed) != len(expected):
            raise ValueError("Length of observed and expected values must be equal")

        component = [((obs-exp)**2.0)/exp for obs, exp in zip(observed, expected)]
        chi_square = sum(component)

        self.__outputs__["Chi-Square"] = chi_square

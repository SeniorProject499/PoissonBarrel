"""
std_dev.py: Class StandardDeviation
    Class for Standard Deviation statistical method.
"""

from stats.base import StatisticalMethod


"""Statistical method for computing Standand Deviation.
"""
class StandardDeviation(StatisticalMethod):
    __stat_name__ = "Standard Deviation"

    """Setup inputs and outputs.
    """
    def __init__(self):
        super(StandardDeviation, self).__init__()
            
        self.__inputs__ = {
            "Values": None
        }

        self.__outputs__ = {
            "Std Dev Value": None
        }


    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "Values":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))


    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:
        nums = self.__inputs__["Values"]
        mean = sum(nums)/len(nums)
        data = [(num - mean) ** 2 for num in nums]
        variance = sum(data)/(len(data) - 1)
        std = variance ** 0.5

        self.__outputs__["Std Dev Value"] = std

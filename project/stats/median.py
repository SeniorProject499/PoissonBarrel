"""
median.py: Class Median
    Class for Median statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Median.
"""
class Median(StatisticalMethod):
    __stat_name__ = "Median"
    
    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self) -> None:
        super(Median, self).__init__()
    
        self.__inputs__ = {
            "Values" : None
        }
    
        self.__outputs__ = {
            "Median Value" : None              
        }
        
    
    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == 'Values':
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))

    
    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:
        values = sorted(list(self.__inputs__["Values"]))
        middle = len(values) // 2

        median = 0
        
        if len(values) & 1:
            median = values[middle]
        else:
            median = (values[middle] + values[middle-1]) / 2
        
        self.__outputs__["Median Value"] = median

"""
percentile.py: Class Percentile
    Class for Percentile statistical method.
"""

from stats.base import StatisticalMethod
from math import ceil


"""The statical method class for Percentile.
"""
class Percentile(StatisticalMethod):
    __stat_name__ = "Percentile"
    
    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(Percentile, self).__init__()
        
        self.__inputs__ = {
            "Values": None,
            "Percentiles": None
        }
        
        self.__outputs__ = {
            "Results": None
        }
        
    
    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value ) -> None:
        if name == "Values":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))
        elif name == "Percentiles":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))
            if not all(v == int(v) for v in value):
                raise TypeError("Input '{}' contains a non-integer".format(name))
            if any(v > 100 or v < 0 for v in value):
                raise ValueError("All values in '{}' must be between 0 and 100".format(name))


    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:

        """Find the value at the percentile in the list of values.
        """
        def value_at_percentile(values, p: float):
            return values[ceil(p * len(values))-1]


        values = sorted(self.__inputs__["Values"])
        ps = self.__inputs__["Percentiles"]

        results = [value_at_percentile(values, p/100) for p in ps]
        self.__outputs__["Results"] = results

"""
sign_test.py: Class SignTest
    Class for Sign Test statistical method.
"""

from stats.base import StatisticalMethod
from math import factorial


"""The statical method class for Sign Test.
"""
class SignTest(StatisticalMethod):
    __stat_name__ = "Sign Test"

    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(SignTest, self).__init__()

        self.__inputs__ = {
            "First": None,
            "Second": None
        }

        self.__outputs__ = {
            "Probability (one-tailed)": None,
            "Probability (two-tailed)": None
        }


    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "First":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))
        if name == "Second":
            if isinstance(value, list):
                if not all(isinstance(v, (int, float)) for v in value):
                    raise TypeError("Input '{}' contains a non-number".format(name))
            elif not isinstance(value, (float, int)):
                raise TypeError("Input '{}' is not a number".format(name))


    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:

        """Calculate number of combinations.
        """
        def nCr(n, r):
            return factorial(n)/factorial(r)/factorial(n-r)


        """Calculate a one-sided probability.
        """
        def calculate_probability(n, r):
            probability = 0.0
            for j in range(r, n+1):
                probability += nCr(n, j) * (0.5 ** j) * (0.5 ** (n-j))

            return probability


        number_set = self.__inputs__["First"]
        hypothesis = self.__inputs__["Second"]

        if isinstance(hypothesis, list):
            inputs = zip(number_set, hypothesis)
        elif isinstance(hypothesis, (float, int)):
            inputs = zip(number_set, [hypothesis] * len(number_set))

        differences = [(a-b) for a, b in inputs]

        num_pos_diffs = len([a for a in differences if a > 0])
        num_neg_diffs = len([a for a in differences if a < 0])
        num_zero_diffs = len([a for a in differences if a == 0])

        n = num_pos_diffs + num_neg_diffs
        r = max(num_pos_diffs, num_neg_diffs)

        one_sided_prob = calculate_probability(n, r)
        two_sided_prob = min(2.0 * one_sided_prob, 1.0)

        self.__outputs__["Probability (one-tailed)"] = one_sided_prob
        self.__outputs__["Probability (two-tailed)"] = two_sided_prob

"""
rank_sum.py: Class RankSum
    Class for Rank Sum statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Rank Sum.
"""
class RankSum(StatisticalMethod):
    __stat_name__ = "Rank Sum"

    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(RankSum, self).__init__()

        self.__inputs__ = {
            "Sample One": None,
            "Sample Two": None
        }

        self.__outputs__ = {
            "Sample One Rank Sum": None,
            "Sample Two Rank Sum": None
        }


    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "Sample One" or name == "Sample Two":
            if not all(isinstance(v, (int,float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))


    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:

        """Method for finding locations of a value in a list.
        """
        def all_occurrences(v, l):
            return [i+1 for i, e in enumerate(l) if e[1] is v[1]]

        """Calculate the rank for a single value given all values.
        """
        def rank(xs, x):
            sorted_values = sorted(xs, key=lambda x: x[1])
            occurrences = all_occurrences(x, sorted_values)
            return (x, sum(occurrences)/len(occurrences))


        for key in self.__outputs__.keys():
            self.__outputs__[key] = None

        for k, v in self.__inputs__.items():
            if v is None:
                raise KeyError("Input '{}' not set yet".format(k))

        # Convert to list tuples with sample markers
        sample_one = [(1, v) for v in self.__inputs__["Sample One"]]
        sample_two = [(2, v) for v in self.__inputs__["Sample Two"]]

        if len(sample_one) != len(sample_two):
            raise ValueError("Samples are different lengths")

        # Combine the two samples
        all_values = sample_one + sample_two

        # Calculate and sort the ranks
        ranks = [rank(all_values, x) for x in all_values]
        ranks = sorted(ranks, key=lambda x: x[1])

        # Split the ranks by sample number
        sample_one_ranks = [x[1] for x in ranks if x[0][0] is 1]
        sample_two_ranks = [x[1] for x in ranks if x[0][0] is 2]

        self.__outputs__["Sample One Rank Sum"] = sum(sample_one_ranks)
        self.__outputs__["Sample Two Rank Sum"] = sum(sample_two_ranks)

"""
mode.py: Class Mode
    Class for Mode statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Mode.
"""
class Mode(StatisticalMethod):
    __stat_name__ = "Mode"

    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(Mode, self).__init__()
        
        self.__inputs__ = {
            'Values' : None
        }
        
        self.__outputs__ = {
            'Mode Value' : None
        }
        

    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) ->None:
        if name == 'Values':
            if not all( isinstance( v, (int, float ) ) for v in value ):
                raise TypeError("Input '{}' contains a non-number".format(name))
    

    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:
        nums = self.__inputs__['Values']

        freqDict = {}
        for v in nums:
            if v in freqDict:
                freqDict[v] += 1
            else:
                freqDict[v] = 1
        
        items = sorted(freqDict.items(), key=lambda t: t[1], reverse=True)
        
        if len(items) > 1 and items[0][1] == items[1][1]:
            raise ValueError("Error no mode for these values")
        
        self.__outputs__['Mode Value'] = items[0][0]

"""
summary.py: Class Summary
    Statistical method summary output class.
    Returns textual summary of statistical measure inputs and outputs.
"""

from time import ctime
from json import dumps

"""Class for handling statistical summaries.
"""
class Summary:

    information = None
    date_created = None

    """Handles initialization.
    """
    def __init__(self, information):
        self.information = information
        self.information["date"] = ctime()

    
    """The textual representation of the summary.
    """
    def __str__(self):
        summary = []

        summary.append("Computed new result @ {}".format(self.information["date"]))
        summary.append("    Statistical measure name: {}".format(self.information["measure"]))

        summary.append("    With the following inputs:");
        for name, value in self.information["inputs"].items():
            summary.append("        {} = {}".format(name, value))

        summary.append("    And the following outputs:");
        for name, value in self.information["outputs"].items():
            summary.append("        {} = {}".format(name, value))

        summary.append("\n")

        return "\n".join(summary)

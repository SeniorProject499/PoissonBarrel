"""
base.py: Class StatisticalMethod
    Base class for statistical methods.
    This contains the abstract methods each statistical measure should implement.
"""

from abc import ABCMeta, abstractmethod
from stats.summary import Summary


"""Base class for statistical methods. This contains the abstract methods each
statistical measure should implement.
"""
class StatisticalMethod:
    __metaclass__ = ABCMeta

    """Handles initialization.
    """
    def __init__(self):
        pass


    """Abstract method for checking input values.
    """
    @abstractmethod
    def checkInput(self, name, value):
        raise NotImplementedError


    """The abstract method that each statistical method will implement to
    compute all of its results.
    """
    @abstractmethod
    def compute(self):
        raise NotImplementedError
    

    """Get a list of input field names.
    """
    def getInputNames(self):
        return sorted(list(self.__inputs__.keys()))
    

    """Get a list of output field names.
    """
    def getOutputNames(self):
        return sorted(list(self.__outputs__.keys()))


    """Method used for setting inputs.
    """
    def setInput(self, name, value):
        if name not in self.__inputs__.keys():
            raise NameError("No such input field: {}".format(name))

        if isinstance(value, list):
            if len(value) is 0:
                raise ValueError("No values provided for {}".format(name))

        self.checkInput(name, value)
        self.__inputs__[name] = value


    """Get an output from the computed results.
    """
    def getOutput(self, name):
        if name not in self.__outputs__.keys():
            raise NameError("No such output '{}'".format(name))

        return self.__outputs__[name]


    """The method that computes results (by calling the subclass compute
    method). This does some extra checks before.
    """
    def computeResult(self):
        for k, v in self.__inputs__.items():
            if v is None:
                raise KeyError("Input '{}' not set yet".format(k))

        self.resetOutputs()
        self.compute()


    """Clear all previous input values.
    """
    def resetInputs(self):
        for key in self.__inputs__.keys():
            self.__inputs__[key] = None


    """Clear all previous output values.
    """
    def resetOutputs(self):
        for key in self.__outputs__.keys():
            self.__outputs__[key] = None


    """Clear all previous input and output values.
    """
    def reset(self):
        self.resetInputs()
        self.resetOutputs()


    """Summarize the results in a textual format.
    """
    def summary(self) -> Summary:
        return Summary({
            "measure": self.__class__.__stat_name__,
            "inputs": self.__inputs__,
            "outputs": self.__outputs__
        })

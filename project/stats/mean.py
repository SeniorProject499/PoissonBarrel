"""
mean.py: Class Mean
    Class for Mean statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Mean.
"""
class Mean(StatisticalMethod):
    __stat_name__ = "Mean"

    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(Mean, self).__init__()

        self.__inputs__ = {
            "Values": None
        }

        self.__outputs__ = {
            "Mean Value": None
        }
            

    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "Values":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))
    

    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:
        nums = self.__inputs__["Values"]
        self.__outputs__["Mean Value"] = sum(nums)/len(nums)

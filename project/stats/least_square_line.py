"""
least_square_line.py: Class LeastSquareLine
    Class for Least Square Line statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Least Square Line.
"""
class LeastSquareLine(StatisticalMethod):
    __stat_name__ = "Least Square Line"
    
    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(LeastSquareLine, self).__init__()
    
        self.__inputs__ = {
            "X Values" : None,
            "Y Values" : None               
        }
        
        self.__outputs__ = {
            "Slope" : None,
            "Y-Intercept" : None
        }
        
        
    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "X Values" or name == "Y Values":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))
    
    
    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:
        x_values = self.__inputs__["X Values"]
        y_values = self.__inputs__["Y Values"]

        if len(x_values) != len(y_values):
            raise TypeError("Different number of X and Y values")
        
        sum_x = sum(x_values)
        sum_y = sum(y_values)

        sum_of_sqrs = sum(x**2 for x in x_values)
        sum_of_products = sum(x*y for x, y in zip(x_values, y_values))

        rise = len(x_values) * sum_of_products - sum_x * sum_y
        run = len(x_values) * sum_of_sqrs - sum_x ** 2
        
        slope = rise/run
        y_intercept = (sum_y - slope * sum_x)/len(x_values)
        
        self.__outputs__["Slope"] = slope
        self.__outputs__["Y-Intercept"] = y_intercept

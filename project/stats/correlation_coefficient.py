"""
correlation_coefficient.py: Class CorrelationCoefficient
    Class for Correlation Coefficient statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Correlation Coefficient.
"""
class CorrelationCoefficient(StatisticalMethod):
    __stat_name__ = "Correlation Coefficient"

    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(CorrelationCoefficient, self).__init__()

        self.__inputs__ = {
            "X Values": None,
            "Y Values": None
        }

        self.__outputs__ = {
            "Result": None
        }


    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "X Values" or name == "Y Values":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))


    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:

        """Function for calculating the deviation of a value from the mean.
        """
        def deviation(value: (int, float), values: list) -> (int, float):
            return (value - sum(values)/len(values))


        x_values = self.__inputs__["X Values"]
        y_values = self.__inputs__["Y Values"]

        if len(x_values) != len(y_values):
            raise TypeError("The len(X Values) != len(Y Values)")

        x_devs = [deviation(x, x_values) for x in x_values]
        y_devs = [deviation(y, y_values) for y in y_values]

        pod_sum = sum((xd * yd) for xd, yd in zip(x_devs, y_devs))

        x_devs_sqrd_sum = sum(x**2.0 for x in x_devs)
        y_devs_sqrd_sum = sum(y**2.0 for y in y_devs)

        result = pod_sum/((x_devs_sqrd_sum * y_devs_sqrd_sum) ** 0.5)

        self.__outputs__["Result"] = result

"""
binomial_distribution.py: Class BinomialDistribution
    Class for Binomial Distribution statistical method.
"""

from stats.base import StatisticalMethod
from math import factorial


"""The statical method class for Binomial Distribution.
"""
class BinomialDistribution(StatisticalMethod):
    __stat_name__ = "Binomial Distribution"
    
    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self) -> None:
        super(BinomialDistribution, self).__init__()
        
        self.__inputs__  = {      
            "Number of Trials" : None,
            "Number of Successes" : None,
            "Probability of Success" : None
        }
        
        self.__outputs__ = {
            "Distribution" : None
        }

        
    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == 'Number of Trials':
            if not all(isinstance(v, (int,float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))

        elif name == 'Number of Succeses':
            if not all(isinstance(v, (int,float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))
            if not all(v == int(v) for v in value):
                raise TypeError("Input '{}' contains a non-integer".format(name))
            if any(v <= 0 for v in value):
                raise ValueError("All values in '{}' must be >= 0".format(name))
        
        elif name == 'Probability of Success':
            if len(value) != 1:
                raise ValueError("There should only be one value for '{}'".format(name))
            
            v = value[0]

            if not isinstance(v, float):
                raise TypeError("Input '{}' must be a float".format(name))
            if v > 0.0 or v < 1.0:
                raise ValueError("Input '{}' must be between 0.0 and 1.0".format(name))

    
    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:

        """Calculate number of combinations.
        """
        def nCr(n: int, r: int) -> int:
            return factorial(n)/factorial(r)/factorial(n-r)
        

        """Calculate the distribution.
        """
        def distribution(n: int, p: float, v: int) -> float:
            return nCr(n, v) * p ** v * (1 - p) ** (n - v)
        

        num_trials = self.__inputs__['Number of Trials'][0]
        successes = self.__inputs__['Number of Successes']
        prob = self.__inputs__['Probability of Success'][0]

        dists = [distribution(num_trials, prob, x) for x in successes]
        self.__outputs__[ 'Distribution' ] = dists

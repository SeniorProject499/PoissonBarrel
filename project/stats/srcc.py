"""
srcc.py: SpearmanRankCorrelationCoefficent
    Class for Spearman Rank Correlation Coefficent statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Spearman Rank Correlation Coefficent.
"""
class SpearmanRankCorrelationCoefficent(StatisticalMethod):
    __stat_name__ = "Spearman Rank Correlation Coefficent"

    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(SpearmanRankCorrelationCoefficent, self).__init__()

        self.__inputs__ = {
            "X Values": None,
            "Y Values": None
        }

        self.__outputs__ = {
            "Result": None
        }


    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, value) -> None:
        if name == "X Values" or name == "Y Values":
            if not all(isinstance(v, (int, float)) for v in value):
                raise TypeError("Input '{}' contains a non-number".format(name))


    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self) -> None:

        """Method for finding locations of a value in a list.
        """
        def all_occurrences(v, l):
            return [i+1 for i, e in enumerate(l) if e is v]

        """Calculate the rank for a single value given all values.
        """
        def rank(xs, x):
            sorted_values = sorted(xs, reverse=True)
            occurrences = all_occurrences(x, sorted_values)
            return (x, sum(occurrences)/len(occurrences))


        for key in self.__outputs__.keys():
            self.__outputs__[key] = None

        for k, v in self.__inputs__.items():
            if v is None:
                raise KeyError("Input '{}' not set yet".format(k))

        x_values = self.__inputs__["X Values"]
        y_values = self.__inputs__["Y Values"]
        
        # check that the lists are the same length
        if len(x_values) != len(y_values):
            raise TypeError("The len(X Values) != len(Y Values)")

        # Calculate the ranks
        x_ranks = [rank(x_values, x) for x in x_values]
        y_ranks = [rank(y_values, y) for y in y_values]

        rank_diffs = [(x_rank[1] - y_rank[1]) for x_rank, y_rank in zip(x_ranks, y_ranks)]

        n = len(x_values)
        result = 1.0 - (6.0 * sum(rd**2.0 for rd in rank_diffs)/((n**3.0) - n))
        self.__outputs__["Result"] = result

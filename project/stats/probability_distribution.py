"""
probability_distribution.py: Class Probability_Dist
    Class for Probability Distribution statistical method.
"""

from stats.base import StatisticalMethod


"""The statical method class for Probability Distribution.
"""
class ProbabilityDistribution(StatisticalMethod):
    __stat_name__ = "Probability Distribution"
    
    """Class initializer. This sets up the required input and output fields.
    """
    def __init__(self):
        super(ProbabilityDistribution, self).__init__()
        
        self.__inputs__ = {
            "Values" : None
        }
        
        self.__outputs__ = {
            "Distribution" : None                          
        }
        

    """Set an input. This will verify that the value passed is okay.
    """
    def checkInput(self, name: str, values ) -> None:
        if name == 'Values':
            if not all(isinstance(v, (int, float)) for v in values):
                raise TypeError("Input '{}' contains a non-number".format(name))
        
        
    """Compute the statistical results. Store the result in the outputs.
    """
    def compute(self):
        values = self.__inputs__["Values"]
        dists = [value/sum(values) for value in values]

        self.__outputs__["Distribution"] = dists

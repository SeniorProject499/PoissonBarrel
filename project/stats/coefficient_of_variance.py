from stats.base import StatisticalMethod

class Coefficient_Of_Variance( StatisticalMethod ):
    __stat_name__ = 'Coefficient of Variance'
    
    
    def __init__(self):
        
        self.__inputs__ = { 'Values': None }
        self.__outputs__ = { 'Coefficient Value': None }
        
    
    def checkInput(self, name:str, value ) -> None:
        
        if name == 'Values':
            if not all( isinstance( v, (int, float ) ) for v in value ):
                raise TypeError( "Inputs '{}' contains a non-number".format( name ) )
            
    
    def compute(self) -> None:
        
        nums = self.__inputs__[ 'Values' ]
        mean = sum( nums ) / len( nums )
        
        data = [ ( num - mean ) ** 2 for num in nums ]
        variance = sum(data)/( len( data ) - 1 )
        std = variance ** 0.5
        
        self.__outputs__[ 'Coefficient Value' ] = std / mean
        
        
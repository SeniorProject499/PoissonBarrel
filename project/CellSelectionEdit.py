"""
CellSelectionEdit.py: Class CellSelectionEdit
	Handling for editing of table cell selection for statistical measure input.
"""

from PyQt5 import QtWidgets

from TableError import TableError


class CellSelectionEdit( QtWidgets.QLineEdit ):
    
    def __init__(self, parentDialog ):
        
        super( CellSelectionEdit, self ).__init__(  )
        
        self.parentDialog = parentDialog
        
        self.statMeasure = None
        
        self.statInputStr = None
        
        self.tableModel = None
        
        self.textChanged.connect( self.text_Changed )
        
        
        
    def setStatMeasureInput( self, statMeasure, statInputStr, tableModel ):
        
        self.statMeasure = statMeasure
        self.statInputStr = statInputStr
        self.tableModel = tableModel
        
        
    def focusInEvent(self, e ):
        
        super( CellSelectionEdit, self ).focusInEvent( e )
        
        self.parentDialog.notifySelEditFocus( self, True  )
        
    def text_Changed( self ):
        
        self.parentDialog.clearErrorText()
        
        if self.statMeasure == None or self.statInputStr == None:
            return
        
        try:
            vals = self.tableModel.getNumericData( self.text() )
            self.statMeasure.setInput( self.statInputStr, vals  )
        
        except Exception as err:
            self.parentDialog.displayErrorText(str(err))
            text = self.text()
            self.setStyleSheet( "background-color: rgb( 255, 100, 100);")
            return 
        
        self.setStyleSheet( "background-color: rgb(100,220, 100 );")

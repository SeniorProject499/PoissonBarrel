"""
CSV_Writer.py: Class CSV_Writer
	Function to write table contents to CSV file.
"""

import csv


class CSV_Writer( object ):
    
    def __init__( self ):
        
        pass
        
    def writeFile(self, fileName, data ):
        
        #open the file for writing
        
        try:
            f = open( fileName, 'w', newline = '' )
            
        except:
            
            return False
        
        #create the writer
        writer = csv.writer( f, delimiter = ',', quotechar='"', quoting = csv.QUOTE_MINIMAL )
        
        
        #write each row
        for row in data:
            writer.writerow( row )
            
        #close the file
        f.close()
        
        return True
        
        
        
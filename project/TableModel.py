"""
TableModel.py: Class TableModel
	Model and functions for spreadsheet table and cells.
"""

#from TableView import TableView
from PyQt5.QtWidgets import QTableWidgetItem, QTableWidgetSelectionRange
from TableWidget import TableWidget
import TableParsing
from TableError import TableError

class TableModel( object ):
    
    def __init__(self, appModel ):
        
        #set the application model
        self.appModel = appModel
        
        #create the table widget
        self.tableWidget = TableWidget( self )
        
        #the default number of rows and columns
        self.defaultNumRows = 26
        self.defaultNumCols = 26
        
        #set the number of row and columns
        self.numRows = self.defaultNumRows
        self.numCols = self.defaultNumCols
        
        self.tableWidget.setRowCount( self.defaultNumRows )
        self.tableWidget.setColumnCount( self.defaultNumCols )
        
        #set the column headings to alphabetic characters
        self.setColumnHeadings()
        
        #reference to the currently opened dialog
        self.dialogWidget = None
        
        self.cellDoubleClkd = False
        
        #maps cell indices to numeric data within the table
        self.numericDict = {}
        
        #the number of decimal places to be displayed
        self.decPrec = 4
        
        self.updateCellItem = True
        
        #True if the user changes the value of a cell
        self.isModified = False
        
    def modified(self):
    	return self.isModified
    
    def setModified(self, isModified ):
    	self.isModified = isModified    
        
    def getTableWidget(self):
        return self.tableWidget
    
    def setActiveDialog( self, dialogWidget ):
        self.dialogWidget = dialogWidget
    
    
    '''set the column headings to alphabetic characters'''
    def setColumnHeadings( self ):
        
        for colNum in range( self.numCols  ):
            
            headingStr = TableParsing.colNum2Heading( colNum )
            
            self.tableWidget.setHorizontalHeaderItem( colNum, QTableWidgetItem( headingStr ) )
    
    
    '''removes all data contained in the cells, and resizes 
       the table to its default number of row and columns'''
    def resetTable(self):
        
        self.numRows = self.defaultNumRows
        self.numCols = self.defaultNumCols
        
        self.tableWidget.clear()
        self.numericDict = {}
        
        self.isModified = False
        
        self.tableWidget.setRowCount( self.numRows )
        self.tableWidget.setColumnCount( self.numCols )
        
        self.setColumnHeadings()
    
    
    '''loads csv data into the table '''
    def buildTable( self, csvData ):
        #clear out any cells within the table that contain text
        self.tableWidget.clear()
        
        #reset the number of row and columns
        self.numRows = self.defaultNumRows
        self.numCols = self.defaultNumCols
        
        
        #get the number rows
        numRows = len( csvData )
        
        #determine the number rows that will be displayed
        if numRows > self.numRows:
            self.numRows = numRows
        
        #loop thru each row to determine the number of columns to be displayed
        for row in csvData:
            
            numCols = len( row )
            
            if numCols > self.numCols:
                self.numCols = numCols
        
        #sets the table's row and column count        
        self.tableWidget.setRowCount( self.numRows )
        self.tableWidget.setColumnCount( self.numCols )
        
        #set the column headings to alphabetic characters
        self.setColumnHeadings()
        
        rowNum = 0
        
        #loop thru each row of the csv table data
        for row in csvData:
            colNum = 0
            
            #loop through each row entry
            for entry in row:
            	
            	try:
            		val = float( entry )
            		self.tableWidget.setItem( rowNum, colNum, ( QTableWidgetItem( str( round( val, self.decPrec  ) ) ) ) )
            		self.numericDict[ ( rowNum, colNum ) ] = val
            		
            	except:
            		#insert the string into the corresponding table cell
            		self.tableWidget.setItem( rowNum, colNum, QTableWidgetItem( entry ) )
            		
            	colNum += 1
                
            rowNum += 1
        
        self.isModified = False
                
            
    def extractTableData(self):
        '''
        Method Name: extractTableData()
        Desc: extracts the data contained in each of cell of the table
        Returns: list of rows where each row is list of string values representing the
                 data contained each cell of the row
        '''
        tableData = []
        
        
        #loop from 0 to the number of rows in the table
        for rowNum in range( self.numRows ):
            
            #create a list containing the data from each cell of the row
            rowData = []
            
            #loop from 0 to the number of columns in the table
            for colNum in range( self.numCols ):
            	
            	
            	if ( rowNum, colNum ) in self.numericDict:
            		rowData.append( str( self.numericDict[ ( rowNum, colNum ) ] ) )
            		continue
            	
            	#get the cell item corresponding to the row and column number
            	cellItem = self.tableWidget.item( rowNum, colNum )
            	
            	#if the cell item does not exist, the insert a empyt string
            	if not cellItem:
            	 	rowData.append( '' )
            	else:
                    #otherwise retrieve the text contained in the cell
                    rowData.append( cellItem.text() )
                
            #append the row
            tableData.append( rowData )

        # Remove any empty rows from the bottom of the table.
        for index, row in reversed(list(enumerate(tableData))):
            if not any(row):
                del tableData[index]
            else:
                break

        # Calculate the number of columns to have. Do this finding the furthest
        # non-empty item out from all of the rows.
        numColumns = 0
        for row in tableData:
            reversedItems = (i for i, e in enumerate(reversed(row)) if e)
            lastItemIndex = len(row) - next(reversedItems, len(row))
            numColumns = max(numColumns, lastItemIndex)

        # Finally, trim off excess columns from each row.
        for i, _ in enumerate(tableData):
            tableData[i] = tableData[i][:numColumns]


        return tableData
    
    
    #given the row and column number this function returns
    #data contained in the specified cell as string
    def getCellStr(self, rowNum, colNum ):
        
        cellItem = self.tableWidget.item( rowNum, colNum )
        
        if cellItem:
            return cellItem.text()
        
        return ''
    
    #given a row and column number this function sets
    #the specified cell's data
    def setCellStr(self, rowNum, colNum, dataStr ):
        
        cellItem = self.tableWidget.item( rowNum, colNum )
        
        if cellItem:
            
            cellItem.setText( dataStr )
            
        else:
            self.tableWidget.setItem( rowNum, colNum, QTableWidgetItem( dataStr ) )
    
    #this function returns a list tuples containing cell indices
    #the 1st and 2nd elements in each tuple correspond to the
    #cell's row and column number
    def getSelectedCells(self):
        
        indices = self.tableWidget.selectedIndexes()
        
        cells = []
        
        for i in indices:    
            cells.append( ( i.row(), i.column() ) )
            
        return cells
    
    
    #this method deselects all cells in the table
    def deselectCells(self ):
        
        indices = self.getSelectedCells()
        
        for rowNum, colNum in indices:
            
            selRange = QTableWidgetSelectionRange( rowNum, colNum, rowNum, colNum )
            
            self.tableWidget.setRangeSelected( selRange, False )
    
    
    #given a cell heading string this function selects all cells
    #within the heading
    def setSelectedCells( self, cellHeading ):
        
        #determine if cell headings are valid by converting to indices
        indices = TableParsing.heading2Cells( cellHeading )
        if not indices:
            return
        
        #deselect all cells that currently selected
        self.deselectCells()
        
        #select each cell 
        for colNum, rowNum in indices:
            
            selRange = QTableWidgetSelectionRange( rowNum, colNum, rowNum, colNum )
            
            self.tableWidget.setRangeSelected( selRange, True )
            
        #active the table widget
        self.tableWidget.activateWindow()
        
    
    #given a string of cell headings this method attempts to
    #extract the data contained in each cell and covert it
    #to a float
    #if successful this method will return a list of floats
    #otherwise will raise a table exception
    def getNumericData(self, headingStr):
        
        indices = TableParsing.heading2Cells( headingStr )
        
        if not indices:
            raise TableError( headingStr + ' is not a valid group of cells' )
        
        #indices that are out of bounds
        outOfBoundsLst = []
        
        #list of indices whose cell data cannot be converted to floats
        nonConvertableLst = []
        
        #indices that cannot be converted to floats
        fltLst = []
        
        for colNum, rowNum in indices:
            
            
            #detect an attempt to read cell that are out-of-bounds
            if rowNum >= self.numRows or colNum >= self.numCols:
                
                outOfBoundsLst.append( ( rowNum, colNum ) )
                continue
            
            '''
            data = self.getCellStr(rowNum, colNum)
            
            try:
                fltLst.append(float(data))
                
            except:
                nonConvertableLst.append( ( rowNum, colNum ) )'''
            index = ( rowNum, colNum )
            
            if index in self.numericDict:
            	fltLst.append( self.numericDict[ index ] )
            
            else:
            	nonConvertableLst.append( index )
            
        
        #determine if an error occurred
        if len( outOfBoundsLst ) > 0 or len( nonConvertableLst ) > 0:
            
            errMsg = ''
            
            if len( outOfBoundsLst ) > 0:
                
                #get the cell heading of the indices that were out of bounds
                heading = TableParsing.cells2Heading( outOfBoundsLst )
                
                errMsg = 'Cells: ' + heading + ' are out of bounds.'
                
            
            if len( nonConvertableLst ) > 0:
                
                if errMsg != '':
                    errMsg += '\n'
                
                #get the cell heading of indices that could not be converted to floats
                heading = TableParsing.cells2Heading( nonConvertableLst  )
                
                errMsg += 'Cells: ' + heading + ' cannot be converted to numeric values.'
                
                
            raise TableError( errMsg )
        
        return fltLst
    

    def getNumericRegionData(self, headingStr):


        """Function for checking if all of the rows are next to each other.
        """
        def allRowsAdjacent(rows):
            diffs = [rows[i+1] - rows[i] for i in range(len(rows)-1)]
            return all(diff == 1 for diff in diffs)

        
        indices = TableParsing.heading2Cells( headingStr )
        
        if not indices:
            raise TableError( headingStr + ' is not a valid group of cells' )
        
        #indices that are out of bounds
        outOfBoundsLst = []
        
        #list of indices whose cell data cannot be converted to floats
        nonConvertableLst = []
        
        #indices that cannot be converted to floats
        rows = list(set(i[1] for i in indices))
        numberOfRows = len(rows)

        if not allRowsAdjacent(rows):
            raise TableError("Not all rows are adjacent")

        fltLst = []
        for _ in range(numberOfRows):
            fltLst.append([])
        
        for colNum, rowNum in indices:
            
            
            #detect an attempt to read cell that are out-of-bounds
            if rowNum >= self.numRows or colNum >= self.numCols:
                outOfBoundsLst.append((rowNum, colNum))
                continue
            
            index = (rowNum, colNum)
            
            if index in self.numericDict:
            	i = numberOfRows - rowNum
            	fltLst[i].append(self.numericDict[ index ])
            else:
            	nonConvertableList.append( index )
            
        
        #determine if an error occurred
        if len( outOfBoundsLst ) > 0 or len( nonConvertableLst ) > 0:
            
            errMsg = ''
            
            if len( outOfBoundsLst ) > 0:
                
                #get the cell heading of the indices that were out of bounds
                heading = TableParsing.cells2Heading( outOfBoundsLst )
                
                errMsg = 'Cells: ' + heading + ' are out of bounds.'
                
            
            if len( nonConvertableLst ) > 0:
                
                if errMsg != '':
                    errMsg += '\n'
                
                #get the cell heading of indices that could not be converted to floats
                heading = TableParsing.cells2Heading( nonConvertableLst  )
                
                errMsg += 'Cells: ' + heading + ' cannot be converted to numeric values.'
                
                
            raise TableError( errMsg )
        fltLst.reverse()
        return fltLst
    
    #given a string contain cell headings this method
    #return a list non-empty cell data strings
    #raises a table exception if any of the cell
    #data contains a string which is empty or is filled with white space
    def getStringData(self, headingStr ):
        
        indices = TableParsing.heading2Cells( headingStr )
        
        if not indices:
            raise TableError( headingStr + ' is not a valid group of cells.' )
        
        outOfBoundsLst = []
        emptyCellLst = []
        
        cellStrs = []
        
        whiteSpcChrs = [ ' ', '\n', '\t' ]
        
        for colNum, rowNum in indices:
            
            #detect out of bounds cell
            if rowNum >= self.numRows or colNum >= self.numCols:
                outOfBoundsLst.append( ( rowNum, colNum ) )
                continue
            
            data = self.getCellStr(rowNum, colNum )
            
            
            #determine if the cell data contains non-white space
            emptyCell = True
            
            for ch in data:
                    
                if not ch in whiteSpcChrs:
                    emptyCell = False
                    break
            
            if emptyCell:
                emptyCellLst.append( ( rowNum, colNum ) )
                
            else:
                cellStrs.append( data )
                    
        #determine if an exception should be raised
        if len( outOfBoundsLst ) > 0 or len( emptyCellLst ):
            
            errMsg = ''
            
            if len( outOfBoundsLst ) > 0:
                
                heading = TableParsing.cells2Heading( outOfBoundsLst )
                
                errMsg = 'Cells ' + heading + ' are out of bounds.'
                
            
            if len( emptyCellLst ):
                
                if len( errMsg ) > 0:
                    errMsg += '\n'
                    
                heading = TableParsing.cells2Heading( emptyCellLst )
                
                errMsg = 'Cells ' + heading + ' are empty.'
                
                
            raise TableError( errMsg )
        
        return cellStrs
    
    
    #called when the user double clicks a cell in the table widget
    def onCellDoubleClicked(self):
        
        self.cellDoubleClkd = True
        
    
    #if a dialog is currently active this method
    #notifies the dialog when cells are selected
    def onSelectionChange(self):
        if self.dialogWidget:
            self.dialogWidget.update()
    
    #called when the user releases the left mouse button
    #after it was previously down
    def onMouseRelease(self):
        
        if self.dialogWidget and not self.cellDoubleClkd:
            self.dialogWidget.onSelectionTerminated()
            
        self.cellDoubleClkd = False
    
    #called when a cell item is modified
    def onItemChanged(self, curItem ):
    	
    	if not self.updateCellItem:
    		return
    	
    	self.isModified = True
    	
    	rowI = curItem.row()
    	colI = curItem.column()
    	
    	index = ( rowI, colI  )
    	
    	try:
    		val = float( curItem.text() )
    		self.numericDict[ index ] = val
    		self.updateCellItem = False
    		self.setCellStr( rowI, colI, str( round( val, self.decPrec ) ) )
    		self.updateCellItem = True
    		
    	except:
     		if index in self.numericDict:
     			del self.numericDict[ index ]
        
    def addRows(self, numRows ):
    	
    	if numRows <= 0:
    		return
    	
    	self.numRows += numRows
    	self.tableWidget.setRowCount( self.numRows  )
    	
    def addColumns(self, numCols ):
    	if numCols <= 0:
    		return
    	
    	self.numCols += numCols
    	self.tableWidget.setColumnCount( self.numCols )
    	self.setColumnHeadings()
    	
    	
    def setDecPrecision(self, decPrec ):
    	
    	if self.decPrec == decPrec:
    		return
    	
    	self.decPrec = decPrec
    	
    	for index, val in self.numericDict.items():
    	 	rowNum, colNum = index
    	 	dataStr = str( round( val, self.decPrec ) )
    	 	self.setCellStr(rowNum, colNum, dataStr)
    
    def getDecPrecision(self):
    	return self.decPrec
